﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;
using System.Xml;
using System.Linq;
using System.Xml.Serialization;

namespace mini2d_demo
{
    class XMLDocManager
    {
        #region PROPERTIES
        ViewModel vm;
        XmlDocument doc;
        //TreeView tree;

        /// <summary>
        /// File variables
        /// </summary>
        private string asset_path;
        private string destination_path;
        private string project_path;
        private string project_scene_path;
        #endregion

        #region CONSTRUCTOR
        public XMLDocManager() {
            this.doc = new XmlDocument();
            this.vm = new ViewModel();
            //this.tree = new TreeView();
            this.asset_path = Directory.GetParent(@"../../").FullName + @"/assets/";
            this.destination_path = Directory.GetParent(@"../../").FullName + @"/projects/";
            this.project_path = this.destination_path + Globals.Name + ".xml";
            this.project_scene_path = this.destination_path + Globals.Name + ".scene";
            this.LoadFile();
        }
        #endregion

        #region SETUP
        /// <summary>
        /// Load the project file
        /// </summary>
        public void LoadFile()
        {
            if (File.Exists(this.project_path))
            {
                try
                {
                    this.doc.Load(this.project_path);
                }
                catch (Exception ex)
                {
                    Debug.dd("Loading XML File", ex.Message);
                }
            }
        }
        #endregion

        #region SETUP CURRENT LEVEL
        /// <summary>
        /// Set current level after opening a project
        /// </summary>
        /// <param name="pname"></param>
        /// <returns></returns>
        public LevelViewModel SetLevel(string pname)
        {
            LevelViewModel level = new LevelViewModel();

            try
            {
                this.project_path = this.destination_path + pname + ".xml";
                this.LoadFile();

                XmlElement root = this.doc.DocumentElement;
                XmlNodeList elemlist = root.GetElementsByTagName("level");

                XmlNode levelNode = elemlist.Item(0);

                level = new LevelViewModel { Width = int.Parse(levelNode.Attributes["width"].Value), Height = int.Parse(levelNode.Attributes["height"].Value) };

                return level;

            }
            catch (Exception ex)
            {
                Debug.dd("GetLevelCount", ex.Message);
            }

            return level;
        }
        #endregion

        #region TREE STUFF
        /// <summary>
        /// Bind XML data to tree
        /// </summary>
        /// <param name="treeview"></param>
        //public void FormTreeView(TreeView treeview)
        //{

        //    if (!string.IsNullOrEmpty(Globals.Name))
        //    {
        //        this.tree = treeview;

        //        try
        //        {
        //            // Prepare the soil
        //            XmlDocument xmldoc = new XmlDocument();
        //            string p = this.destination_path + "LevelTest.xml";
        //            //FileStream fs = new FileStream(p, FileMode.Open, FileAccess.Read);

        //            xmldoc.Load(p);

        //            // Form the Seed 
        //            XmlDataProvider provider = treeview.FindResource("xmlprovider") as XmlDataProvider;
        //            provider.Document = xmldoc;
        //            provider.XPath = "root/project";
        //            //provider.Refresh();
        //            // Let the tree Germinate
        //            treeview.DataContext = provider;

        //        }
        //        catch (Exception ex)
        //        {
        //            MessageBox.Show(ex.Message);
        //        }
        //    }
        //}
        #endregion

        #region CREATE NEW PROJECT
        /// <summary>
        /// Create a New project, Check if the global project name has already been set
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        public bool CreateNewProject(Project project)
        {
            string path = this.destination_path + project.Name + ".xml";

            if (Globals.Init() || string.Equals(Globals.Name, project.Name) || File.Exists(path))
            {
                return false;
            }
            else
            {
                Globals.Name = project.Name;
                Globals.Type = project.Type;
                Globals.Init();
                this.CreateNewProjectFile(project);
                return true;
            }
        }

        #endregion

        #region CREATE NEW PROJECT FILE
        /// <summary>
        /// Create a new Project XML file
        /// </summary>
        /// <param name="project"></param>
        private void CreateNewProjectFile(Project project) {
            try
            {
                string filename = this.destination_path + project.Name + ".xml";
                XmlTextWriter writer = new XmlTextWriter(filename, System.Text.Encoding.UTF8);
                writer.WriteStartDocument(true);
                    writer.Formatting = Formatting.Indented;
                    writer.Indentation = 2;
                    writer.WriteStartElement("root");
                        writer.WriteStartElement("project");
                            writer.WriteAttributeString("xmlns", "name", null, project.Name);
                            writer.WriteAttributeString("xmlns", "type", null, project.Type);
                        writer.WriteEndElement();
                    writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Flush();
                writer.Close();
               
            }
            catch (Exception ex) {
                Debug.dd("CreateNewProjectFile", ex.Message);
            }
       }
        #endregion

        #region NEW LEVEL
        /// <summary>
        /// Create a new level node
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        public bool CreateNewlevel(LevelViewModel level)
        {

            if (!File.Exists(this.project_path))
            {
                return false;
            }
            else
            {
                this.CreateNewLevelNodes(level);
                Globals.Level = level;
                Globals.Init();
                Globals.HasLevels();
                return true;
            }
        }
        #endregion

        #region CREATE NEW LEVEL NODES
        /// <summary>
        /// Create the level node as a child of Project Element and scene node as level child
        /// </summary>
        /// <param name="level"></param>
        private void CreateNewLevelNodes(LevelViewModel level)
        {
            try
            {
                // Define new elements 
                XmlElement levelElement = this.doc.CreateElement("level");
                XmlElement sceneElement = this.doc.CreateElement("scene");

                // Define parent nodes
                XmlNode parentNode = this.doc.SelectSingleNode("root/project");
                XmlNode levelNode = (XmlNode)levelElement;
                
                // For the level element
                levelElement.SetAttribute("name", level.Name);
                levelElement.SetAttribute("order", level.Order);
                levelElement.SetAttribute("width", level.Width.ToString());
                levelElement.SetAttribute("height", level.Height.ToString());
                
                // For Scene level element
                sceneElement.SetAttribute("src", "");
                sceneElement.SetAttribute("color","#000000");

                //Append to parent nodes
                parentNode.AppendChild(levelElement);            
                levelNode.AppendChild(sceneElement);

                // Update the XML file
                this.doc.Save(this.project_path);

            }catch(Exception ex){
                Debug.dd("CreateNewLevelNodes", ex.Message);
            }

        }
        #endregion

        #region ADD SCENE OBJECT NODE UPON UPLOAD
        /// <summary>
        /// Add object nodes to scene
        /// </summary>
        public void AddSceneObjectNode(Asset asset) {
            try
            {
                // Define new elements 
                XmlElement sceneObjectElement = this.doc.CreateElement("object");
                XmlElement sourceElement = this.doc.CreateElement("source");
                XmlElement sizeElement = this.doc.CreateElement("size");
                XmlElement positionElement = this.doc.CreateElement("position");

                // Define parent nodes
                XmlNode parentNode = this.doc.SelectSingleNode("root/project/level/scene");
                XmlNode sceneObjectNode = (XmlNode)sceneObjectElement;

                //For the Object element
                sceneObjectElement.SetAttribute("id", asset.ID.ToString());
                sceneObjectElement.SetAttribute("name", asset.Name);
                sceneObjectElement.SetAttribute("type", asset.Type.ToString());
                
                //For the Source Element
                sourceElement.InnerText = asset.Source;

                //For the Size Element
                sizeElement.SetAttribute("width", asset.Size.Width.ToString());
                sizeElement.SetAttribute("height", asset.Size.Height.ToString());

                //For the Position Element
                positionElement.SetAttribute("x", asset.Position.X.ToString());
                positionElement.SetAttribute("y", asset.Position.Y.ToString());

                //Append to parent nodes
                parentNode.AppendChild(sceneObjectElement);
                sceneObjectNode.AppendChild(sourceElement);
                sceneObjectNode.AppendChild(sizeElement);
                sceneObjectNode.AppendChild(positionElement);

                // Update the XML file
                this.doc.Save(this.project_path);

            }
            catch (Exception ex)
            {
               Debug.dd("AddSceneObjectNode]: ", ex.Message);
            }
        }
        #endregion

        #region ADD OR UPDATE SCENE BACKGROUND NODE
        public void AddSceneBackgroundNode(Asset asset)
        {
            try
            {
                // Define parent nodes
                XmlNode parentNode = this.doc.SelectSingleNode("root/project/level/scene");
                if (parentNode != null)
                {
                   
                    // Update the background Node
                    //XmlNode backgroundNode = this.doc.SelectSingleNode("root/project/level/scene/background");
                    parentNode.Attributes[0].Value = asset.Source; 
                }

                // Update the XML file
                this.doc.Save(this.project_path);
            }
            catch (Exception ex)
            {
                Debug.dd("UpdateSceneBackgroundNode]: ", ex.Message);
            }
        }
        #endregion

        #region SerializeToXML
        public void SerializeToXML(string filename)
        {
            ObservableCollection<SceneObjectViewModel> sceneObjects = MainWindow.main.sceneCollection;

            Debug.dd("Saving this objects", sceneObjects.Count);

            try{

                XmlNode ObjectNode = this.doc.SelectSingleNode("root/project/level/scene");

                ObjectNode.RemoveAll();

                this.CreateMultipleSceneObjectNodes(sceneObjects);
            }
             catch (Exception ex)
            {
                Debug.dd("SerializeToXML: ", ex.Message);
            }
        }
        #endregion

        #region DELETE NODE
        public void DeleteNode(int index) {
            try
            {
                XmlNodeList ObjectNodes = this.doc.SelectNodes("root/project/level/scene/object");
                XmlNode parentNode = this.doc.SelectSingleNode("root/project/level/scene");

                for (int i = 0; i < ObjectNodes.Count; i++)
                {
                    XmlNode node = ObjectNodes.Item(i);

                    if (Int32.Parse(node.Attributes["id"].Value) == index)
                    {
                        parentNode.RemoveChild(node);
                        this.doc.Save(this.project_path);
                        //break;
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.dd("Delete Node: ", ex.Message);
            }
        }
        #endregion

        #region GET OBJECT COUNT
        public int GetCount() {
            return this.doc.SelectNodes("root/project/level/scene/object").Count;
        }
        #endregion

        #region XML TO LEVEL VIEW MODEL OBJECT
        public ObservableCollection<LevelViewModel> XMLToLevelViewObject()
        {
            ObservableCollection<LevelViewModel> level = new ObservableCollection<LevelViewModel>();
            
            try
            {
                XmlNode levelNode = this.doc.SelectSingleNode("root/project/level");

                if (levelNode != null)
                {
                    level.Add(new LevelViewModel
                    {
                        Width = Int32.Parse(levelNode.Attributes["width"].Value),
                        Height = Int32.Parse(levelNode.Attributes["height"].Value)
                    });
                    return level;
                }

            }catch(Exception e)
            {
                Debug.dd("XMLToLevelViewObject", e.Message);
            }
            return level;
        }
        #endregion

        #region XML TO SCENE OBJECT
        public ObservableCollection<SceneObjectViewModel> XMLToSceneObjects()
        {
            ObservableCollection<SceneObjectViewModel> sceneObjects = new ObservableCollection<SceneObjectViewModel>();

            try
            {
                XmlNodeList ObjectNodes = this.doc.SelectNodes("root/project/level/scene/object");

                foreach (XmlNode node in ObjectNodes)
                {

                    if (node != null)
                    {
                        SceneObjectViewModel ob = new SceneObjectViewModel
                            {
                                ID = Int32.Parse(node.Attributes["id"].Value),
                                Name = node.Attributes["name"].Value,
                                Type = Int32.Parse(node.Attributes["type"].Value),
                                Source = node.Attributes["src"].Value,
                            };

                        foreach (XmlNode childnode in node.ChildNodes)
                        {
                            if (childnode != null)
                            {
                                if (childnode.Name == "size")
                                {
                                    ob.Size = new Size { Width = Double.Parse(childnode.Attributes["width"].Value), Height = Double.Parse(childnode.Attributes["height"].Value) };
                                }
                                else if (childnode.Name == "position")
                                {
                                    ob.Position = new Point(Double.Parse(childnode.Attributes["x"].Value), Double.Parse(childnode.Attributes["y"].Value));
                                }

                            }
                        }

                        sceneObjects.Add(ob);

                    }
                }

                return sceneObjects;

            }
            catch (Exception e)
            {
                Debug.dd("XMLtoSceneObjects", e.Message);
            }

            return sceneObjects;
        }
        #endregion

        #region SET BACKGROUND COLOR
        public void SetBackgroundColor(string p)
        {
             XmlNode parentNode = this.doc.SelectSingleNode("root/project/level/scene");

             if (this.doc.SelectSingleNode("root/project/level/scene").Attributes["color"].Value != null)
             {
                 this.doc.SelectSingleNode("root/project/level/scene").Attributes["color"].Value = p;
             }
             else
             {
                 //XmlElement sceneBackgroundElement = this.doc.CreateElement("background");
                 //XmlNode sceneBackgroundNode = (XmlNode)sceneBackgroundElement;

                 // Set the source attribute
                 parentNode.Attributes["src"].Value = "";
                 parentNode.Attributes["color"].Value =  p;

                 // Append to parent nodes
                 //parentNode.AppendChild(sceneBackgroundElement);
             }

             // Update the XML file
             this.doc.Save(this.project_path);
        }
        #endregion

        #region GET BACKGROUND Image
        public string GetBackgroundImage() { 
            return (this.doc.SelectSingleNode("root/project/level/scene") != null) ? this.doc.SelectSingleNode("root/project/level/scene").Attributes["src"].Value : string.Empty;
        }
        #endregion

        #region GET BACKGROUND Color
        public string GetBackgroundColor()
        {
            return (this.doc.SelectSingleNode("root/project/level/scene") != null) ? this.doc.SelectSingleNode("root/project/level/scene").Attributes["color"].Value : "#000000";
        }
        #endregion

        #region REMOVE NODE
        private void RemoveNode(XmlNode node)
        {
            XmlNodeList ObjectNodes = this.doc.SelectNodes("root/project/level/scene/object");
            node.ParentNode.RemoveChild(node);
            this.doc.Save(this.project_path);
        }
        #endregion

        #region CREATE SINGLE SCENE OBJECT NODE
        public void CreateSceneObjectNodes(SceneObjectViewModel asset)
        {
            try
            {
                // Define new elements 
                XmlElement sceneObjectElement = this.doc.CreateElement("object");
                //XmlElement sourceElement = this.doc.CreateElement("source");
                XmlElement sizeElement = this.doc.CreateElement("size");
                XmlElement positionElement = this.doc.CreateElement("position");

                // Define parent nodes
                XmlNode parentNode = this.doc.SelectSingleNode("root/project/level/scene");
                XmlNode sceneObjectNode = (XmlNode)sceneObjectElement;

                //For the Object element
                sceneObjectElement.SetAttribute("id", asset.ID.ToString());
                sceneObjectElement.SetAttribute("name", asset.Name);
                sceneObjectElement.SetAttribute("type", asset.Type.ToString());
                sceneObjectElement.SetAttribute("src", asset.Source);

                //For the Size Element
                sizeElement.SetAttribute("width", asset.Size.Width.ToString());
                sizeElement.SetAttribute("height", asset.Size.Height.ToString());

                //For the Position Element
                positionElement.SetAttribute("x", asset.Position.X.ToString());
                positionElement.SetAttribute("y", asset.Position.Y.ToString());

                //Append to parent nodes
                parentNode.AppendChild(sceneObjectElement);
                sceneObjectNode.AppendChild(sizeElement);
                sceneObjectNode.AppendChild(positionElement);

                // Update the XML file
                this.doc.Save(this.project_path);

            }
            catch (Exception ex)
            {
                Debug.dd("AddSceneObjectNode]: ", ex.Message);
            }
        } 
        #endregion

        #region CREATE MULTIPLE SCENE OBJECT NODE
        public void CreateMultipleSceneObjectNodes(ObservableCollection<SceneObjectViewModel> assets)
        {
            try
            {
                XMLDocManager doc = new XMLDocManager();
                // Define parent nodes
                XmlNode parentNode = this.doc.SelectSingleNode("root/project/level");
                XmlElement sceneElement = this.doc.CreateElement("scene");

                // For Scene level element
                sceneElement.SetAttribute("src", this.GetBackgroundImage());
                sceneElement.SetAttribute("color", this.GetBackgroundColor());

                XmlNode sceneElementNode = (XmlNode)sceneElement;

                foreach (SceneObjectViewModel asset in assets)
                {
                    // Define new elements 
                    XmlElement sceneObjectElement = this.doc.CreateElement("object");
                    //XmlElement sourceElement = this.doc.CreateElement("source");
                    XmlElement sizeElement = this.doc.CreateElement("size");
                    XmlElement positionElement = this.doc.CreateElement("position");
                    
                    XmlNode sceneObjectNode = (XmlNode)sceneObjectElement;

                    //For the Object element
                    sceneObjectElement.SetAttribute("id", asset.ID.ToString());
                    sceneObjectElement.SetAttribute("name", asset.Name);
                    sceneObjectElement.SetAttribute("type", asset.Type.ToString());
                    sceneObjectElement.SetAttribute("src", asset.Source);

                    //For the Size Element
                    sizeElement.SetAttribute("width", asset.Size.Width.ToString());
                    sizeElement.SetAttribute("height", asset.Size.Height.ToString());

                    //For the Position Element
                    positionElement.SetAttribute("x", asset.Position.X.ToString());
                    positionElement.SetAttribute("y", asset.Position.Y.ToString());

                    //Append to parent nodes
                    sceneElementNode.AppendChild(sceneObjectElement);
                    sceneObjectNode.AppendChild(sizeElement);
                    sceneObjectNode.AppendChild(positionElement);
                    this.doc.Save(this.project_path);
                }

                parentNode.AppendChild(sceneElement);

                // Update the XML file
                this.doc.Save(this.project_path);

            }
            catch (Exception ex)
            {
                Debug.dd("AddMultipleSceneObjectNode]: ", ex.Message);
            }
        }
        #endregion

        #region HELPER METHODS
        /// <summary>
        /// set level count after opening a project
        /// </summary>
        /// <param name="pname"></param>
        /// <returns></returns>
        public int GetLevelCount(string pname)
        {
            int count = 0;

            try{

                this.project_path = this.destination_path + pname + ".xml";
                this.LoadFile();

                XmlElement root = this.doc.DocumentElement;
                XmlNodeList elemlist = root.GetElementsByTagName("level");
                count = elemlist.Count;
                return count;

            }catch(Exception ex){
                Debug.dd("GetLevelCount",ex.Message);
            }

            return count;
            
        }
        #endregion

    }
}
