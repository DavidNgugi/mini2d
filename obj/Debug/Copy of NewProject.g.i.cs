﻿#pragma checksum "..\..\Copy of NewProject.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "24AFCD2EA0EF6866655620845BB5E38D"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace mini2d_demo {
    
    
    /// <summary>
    /// NewProject
    /// </summary>
    public partial class NewProject : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 6 "..\..\Copy of NewProject.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCreateProject;
        
        #line default
        #line hidden
        
        
        #line 9 "..\..\Copy of NewProject.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox projectName;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\Copy of NewProject.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox project_type;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\Copy of NewProject.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBoxItem p;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\Copy of NewProject.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBoxItem f;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\Copy of NewProject.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBoxItem e;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\Copy of NewProject.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBoxItem s;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/mini2d_demo;component/copy%20of%20newproject.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\Copy of NewProject.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.btnCreateProject = ((System.Windows.Controls.Button)(target));
            
            #line 6 "..\..\Copy of NewProject.xaml"
            this.btnCreateProject.Click += new System.Windows.RoutedEventHandler(this.btnCreateProject_Click_1);
            
            #line default
            #line hidden
            return;
            case 2:
            this.projectName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.project_type = ((System.Windows.Controls.ListBox)(target));
            return;
            case 4:
            this.p = ((System.Windows.Controls.ListBoxItem)(target));
            return;
            case 5:
            this.f = ((System.Windows.Controls.ListBoxItem)(target));
            return;
            case 6:
            this.e = ((System.Windows.Controls.ListBoxItem)(target));
            return;
            case 7:
            this.s = ((System.Windows.Controls.ListBoxItem)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

