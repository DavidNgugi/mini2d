﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace mini2d_demo
{
    public class ViewModel : INotifyPropertyChanged
    {
        #region Data Members

        /// <summary>
        /// The list of rectangles that is displayed in the Scene.
        /// </summary>
        public ObservableCollection<SceneObjectViewModel> sceneObjects = new ObservableCollection<SceneObjectViewModel>();

        /// <summary>
        /// The display Box 
        /// </summary>
        private ObservableCollection<RectangleViewModel> dragSelectBox = new ObservableCollection<RectangleViewModel>();

        /// <summary>
        /// The levels 
        /// </summary>
        private ObservableCollection<LevelViewModel> levels = new ObservableCollection<LevelViewModel>();

        #endregion Data Members

        /// <summary>
        /// Constructor
        /// </summary>
        public ViewModel()
        {}

        /// <summary>
        /// The list of rectangles that is displayed in the ListBox.
        /// </summary>
        public ObservableCollection<SceneObjectViewModel> SceneObjects
        {
            get
            {
                return sceneObjects;
            }
            set {
                sceneObjects = value;
                OnPropertyChanged("sceneObjects");
            }
        }

        /// <summary>
        /// The draggable selectbox
        /// </summary>
        public ObservableCollection<RectangleViewModel> DragSelectBox
        {
            get
            {
                return dragSelectBox;
            }
            set
            {
                dragSelectBox = value;
                OnPropertyChanged("dragSelectBox");
            }
        }

        /// <summary>
        /// The list of rectangles that is displayed in the ListBox.
        /// </summary>
        public ObservableCollection<LevelViewModel> Levels
        {
            get
            {
                return levels;
            }
            set
            {
                levels = value;
                OnPropertyChanged("levels");
            }
        }

        #region INotifyPropertyChanged Members

        /// <summary>
        /// Raises the 'PropertyChanged' event when the value of a property of the view model has changed.
        /// </summary>
        private void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        /// <summary>
        /// 'PropertyChanged' event that is raised when the value of a property of the view model has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}
