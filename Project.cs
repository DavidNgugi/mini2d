﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mini2d_demo
{
    class Project
    {
        public string Name { get; private set; }
        public string Type { get; private set; }

        public Project(string name, string type) {
            Name = name;
            Type = type;
        }

    }
}
