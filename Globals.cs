﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace mini2d_demo
{
    public static class Globals
    {
        public static Canvas Canvas { get; set; }
        public static LevelViewModel Level { get; set; }
        public static int LevelCount { get; set; }
        public static string Name { get; set; }
        public static string Type { get; set; }
        public static bool Saved { get; set; }
        public static bool Init() { return string.IsNullOrEmpty(Name) ? false : true; }
        public static bool HasLevels() { return (LevelCount > 0) ? true : false; }
        public static void Reset() { Canvas = null; Level = null; Name = null; Type = null; Saved = false; }
    }
}
