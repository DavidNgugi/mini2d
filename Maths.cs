﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mini2d_demo
{
    public static class Maths
    {
        /// <summary>
        /// Interpolation 
        /// </summary>
        /// <param name="flgoal"></param>
        /// <param name="flcurrent"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static double Lerp(double flgoal, double flcurrent, double dt)
        {
            double fldifference = flgoal - flcurrent;
            if (fldifference > dt)
            {
                return flcurrent + dt;
            }
            else if (fldifference < -dt)
            {
                return flcurrent - dt;
            }

            return flgoal;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static int Clamp(this int value, int min, int max)
        {
            return value > max ? max : (value < min ? min : value);
        }

    }
}
