﻿using Microsoft.Win32;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace mini2d_demo
{

    class UIManager: INotifyPropertyChanged
    {
        #region PROPERTIES
        NewProject newproject;
        NewLevel newlevel;
        AboutBox1 aboutwin;
        XMLDocManager doc;
        Scene scene;
        
        /// <summary>
        /// File variables
        /// </summary>
        private string defaultPath = Directory.GetParent(@"../../").FullName + @"\"; 
        private string projectPath = Directory.GetParent(@"../../").FullName + @"\projects";
        private string destPath = Directory.GetParent(@"../../").FullName + @"\assets";
        private string filename;
        private string sourcePath;
        private string sourceFile;
        private string destFile;

        public bool saved { get; set; }

        /// <summary>
        /// Collections
        /// </summary>
        private ObservableCollection<Asset> assets = new ObservableCollection<Asset>();
        private ObservableCollection<Asset> assetList = new ObservableCollection<Asset>();

        // INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        #endregion 

        #region CONSTRUCTOR
        public UIManager() {
            MainWindow.main.Title = Globals.Init() ? Globals.Name + "- Mini2D for Windows desktop" : "Mini2D for Windows desktop";
            this.newproject = new NewProject();
            this.newlevel = new NewLevel();
            this.aboutwin = new AboutBox1();
            this.doc = new XMLDocManager();
            this.scene = new Scene();
            //this.ReadFilesFromFolder();
        }
        #endregion

        #region SETUP
        /// <summary>
        /// Run UI thread
        /// </summary>
        public void Run() {
            MainWindow.main.assetListCollection = assetList;
        }
        #endregion

        #region EVENT HANDLERS
        /// <summary>
        /// Raise Property Changed Event
        /// </summary>
        /// <param name="propName"></param>
        public void RaisePropertyChanged(string propName) {
            PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }
        #endregion

        #region MENU
        /// <summary>
        /// Handle all Menu actions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void RunMenu(object sender, RoutedEventArgs e){

            System.Windows.Controls.MenuItem item  = sender as System.Windows.Controls.MenuItem;
            
            switch(item.Name){
                
                case "newLevel":
                    if (Globals.Init() && !Globals.HasLevels()) { this.CreateNewLevelDialog(); }
                    else if (Globals.HasLevels()){ MessageBox.Show("Only one level is allowed in Mini2D v1.0.0 Beta", "Sorry", MessageBoxButton.OK, MessageBoxImage.Information);}
                    else if (!Globals.Init()) { MessageBox.Show("Create or Open a Project first", "Sorry", MessageBoxButton.OK, MessageBoxImage.Information); }
                    break;
                case "addImage":
                    if (Globals.Init() && Globals.HasLevels()) { this.AddImageDialog(); }
                    else if (!Globals.Init()) { MessageBox.Show("Create or Open a Project first", "Sorry", MessageBoxButton.OK, MessageBoxImage.Information); }
                    else if (Globals.Init() && !Globals.HasLevels()) { MessageBox.Show("Create or Choose a Level first", "Sorry", MessageBoxButton.OK, MessageBoxImage.Information); }
                    break;
                case "addSound":
                    if (Globals.Init() && Globals.HasLevels()) { this.AddSoundDialog(); }
                    else if (!Globals.Init()) { MessageBox.Show("Create or Open a Project first", "Sorry", MessageBoxButton.OK, MessageBoxImage.Information); }
                    else if (Globals.Init() && !Globals.HasLevels()) { MessageBox.Show("Create or Choose a Level first", "Sorry", MessageBoxButton.OK, MessageBoxImage.Information); }
                    break;
                case "playGame":
                    if (Globals.Init() && Globals.HasLevels()) { }
                    else if (!Globals.Init()) { MessageBox.Show("Create or Open a Project first", "Sorry", MessageBoxButton.OK, MessageBoxImage.Information); }
                    else if (Globals.Init() && !Globals.HasLevels()) { MessageBox.Show("Create or Choose a Level first", "Sorry", MessageBoxButton.OK, MessageBoxImage.Information); }
                    break;
                case "pauseGame":
                    if (Globals.Init() && Globals.HasLevels()) { }
                    else if (!Globals.Init()) { MessageBox.Show("Create or Open a Project first", "Sorry", MessageBoxButton.OK, MessageBoxImage.Information); }
                    else if (Globals.Init() && !Globals.HasLevels()) { MessageBox.Show("Create or Choose a Level first", "Sorry", MessageBoxButton.OK, MessageBoxImage.Information); }
                    break;
                case "viewCode":
                    if (Globals.Init()) { }
                    else if (!Globals.Init()) { MessageBox.Show("Create or Open a Project first", "Sorry", MessageBoxButton.OK, MessageBoxImage.Information); MessageBox.Show("Create or Open a Project first"); }
                    else if (Globals.Init() && !Globals.HasLevels()) { MessageBox.Show("Create or Choose a Level first", "Sorry", MessageBoxButton.OK, MessageBoxImage.Information); }
                    break;
                case "viewDesigner":
                    if (Globals.Init()) { }
                    else if (!Globals.Init()) { MessageBox.Show("Create or Open a Project first", "Sorry", MessageBoxButton.OK, MessageBoxImage.Information); }
                    else if (Globals.Init() && !Globals.HasLevels()) { MessageBox.Show("Create or Choose a Level first", "Sorry", MessageBoxButton.OK, MessageBoxImage.Information); }
                    break;
                case "viewDocs":
                    break;
                case "about":
                    aboutwin.ShowDialog();
                    break;
            }
        }
        #endregion

        #region DIALOGS - NEW PROJECT, NEW LEVEL
        /// <summary>
        /// Show new Project window dialog
        /// </summary>
        public void StartNewProjectDialog(){
            try
            {
                NewProject newproject = new NewProject();
                newproject.ShowDialog();
            }catch(Exception ex){
                MessageBox.Show(ex.Message, "An Error occured \n " + ex.Message, MessageBoxButton.OK);
            }
        }

        /// <summary>
        /// Show new Level window dialog 
        /// </summary>
        public void CreateNewLevelDialog()
        {
            try
            {
                NewLevel newlevel = new NewLevel();
                newlevel.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "An Error occured \n " + ex.Message, MessageBoxButton.OK);
            }
        }

        /// <summary>
        /// Show new Level window dialog 
        /// </summary>
        public void ShowAssetTypeDialog()
        {
            try
            {
                //AssetType assettype = new AssetType();
                //assettype.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "An Error occured \n " + ex.Message, MessageBoxButton.OK);
            }
        }
        #endregion

        #region OPEN PROJECT DIALOG
        /// <summary>
        /// Show open project file explorer dialog
        /// </summary>
        public void OpenProjectDialog(){
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "Mini2D Project files (*.xml)|*.xml";
                openFileDialog.Multiselect = false;
                string fpath = Directory.GetParent(@"../../").FullName + @"\projects";
                openFileDialog.InitialDirectory = fpath;
                openFileDialog.Title = "Choose a project file";

                if (openFileDialog.ShowDialog() == true)
                {
                    // To-do
                    // 1. Add Progress bar
                    // 2. Add Scene destruction and new scene creation/loading

                    filename = openFileDialog.FileName;
                    //string targetFilePath = this.GetTargetPath(System.IO.Path.GetFileNameWithoutExtension(filename));

                    Globals.Reset();

                    this.doc = new XMLDocManager();

                    string pname = System.IO.Path.GetFileNameWithoutExtension(filename);

                    Globals.Name = pname;
                    Globals.Level = this.doc.SetLevel(pname);
                    Globals.LevelCount = this.doc.GetLevelCount(pname);
                    Globals.Init();
                    Globals.HasLevels();
                    MainWindow.main.CanvasItem.Width = Globals.Level.Width;
                    MainWindow.main.CanvasItem.Height = Globals.Level.Height;

                    MainWindow.main.Title = pname + " - Mini2D for Windows desktop";

                    this.LoadScene();
                    this.ReadFilesFromFolder();

                    MainWindow.main.CanvasItem.IsEnabled = true;
                    MainWindow.main.CanvasItem.Visibility = Visibility.Visible;

                    MessageBox.Show("Project " + pname + " loaded");

                }

            }
            catch (Exception ex)
            {
                Debug.dd("Open Project Dialog", ex.Message);
                MessageBox.Show(ex.Message, " [Open Project Dialog] File Openning Error", MessageBoxButton.OK);
            }
        }

        #endregion

        #region LOAD SCENE FILE
        public void LoadScene()
        {
            try {
                ObservableCollection<SceneObjectViewModel> so = this.doc.XMLToSceneObjects();
                ObservableCollection<LevelViewModel> le = this.doc.XMLToLevelViewObject();
                string bgImagepath = this.doc.GetBackgroundImage();
                string bgColor = this.doc.GetBackgroundColor();
                this.scene.CreateScene(le,so, bgImagepath, bgColor);
            }
            catch (Exception ex)
            {
                Debug.dd("Load Scene", ex.Message);
                MessageBox.Show(ex.Message, "Load Scene Error", MessageBoxButton.OK);
            }
        }
        #endregion

        #region ADD IMAGE DIALOG
        /// <summary>
        /// Show image file explorer dialog
        /// </summary>
        public void AddImageDialog() {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "Image files (*.png; *.gif; *.jpeg; *.bmp)|*.png; *.gif;*.jpeg; *.bmp";
                openFileDialog.Multiselect = false;
                openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
                openFileDialog.Title = "Choose an Image file";

                string finalDirPath = this.destPath + @"\"+ Globals.Name;

                if (!Directory.Exists(finalDirPath))
                {
                    Directory.CreateDirectory(finalDirPath);
                }

                if (openFileDialog.ShowDialog() == true)
                {
                    filename = openFileDialog.FileName;
                    this.UploadImageAsset(filename, finalDirPath);
                }

            }
            catch (Exception ex)
            {
                Debug.dd("Add New Image", ex.Message);
                MessageBox.Show(ex.Message, "File Openning Error", MessageBoxButton.OK);
            }
        }
        #endregion  

        #region ADD BACKGROUND IMAGE DIALOG
        /// <summary>
        /// Show image file explorer dialog
        /// </summary>
        public void AddBackgroundImageDialog()
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "Image files (*.png; *.gif; *.jpeg; *.bmp)|*.png; *.gif;*.jpeg; *.bmp";
                openFileDialog.Multiselect = false;
                openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
                openFileDialog.Title = "Choose an Image file";

                string finalDirPath = this.destPath + @"\" + Globals.Name;

                if (!Directory.Exists(finalDirPath))
                {
                    Directory.CreateDirectory(finalDirPath);
                }

                if (openFileDialog.ShowDialog() == true)
                {
                    filename = openFileDialog.FileName;
                    this.UploadBackgroundImageAsset(filename, finalDirPath);
                }

            }
            catch (Exception ex)
            {
                Debug.dd("Add New Image", ex.Message);
                MessageBox.Show(ex.Message, "File Openning Error", MessageBoxButton.OK);
            }
        }
        #endregion

        #region UPLOAD IMAGE ASSET
        /// <summary>
        /// Upload Image Asset
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="finalDirPath"></param>
        private void UploadImageAsset(string filename, string finalDirPath)
        {
            string extension = System.IO.Path.GetExtension(filename);
            string targetImagePath = this.GetTargetPath(System.IO.Path.GetFileNameWithoutExtension(filename), extension);

            // Show Select Type dialog 
            //this.ShowAssetTypeDialog();

            Asset t = new Asset { ID = assetList.Count, Name = System.IO.Path.GetFileNameWithoutExtension(filename), Type = 0 };

            if (!assetList.Contains(t))
            {
                sourcePath = System.IO.Path.GetFullPath(filename);
                sourceFile = System.IO.Path.Combine(sourcePath, filename);
                destFile = System.IO.Path.Combine(finalDirPath, filename);

                string filepath = finalDirPath + @"\" + System.IO.Path.GetFileName(filename);

                System.Drawing.Bitmap bm = System.Drawing.Image.FromFile(filename) as System.Drawing.Bitmap;

                // Upload by extension
                switch (extension)
                {
                    case ".bmp":
                        bm.Save(filepath, ImageFormat.Bmp);
                        break;
                    case ".jpg":
                        bm.Save(filepath, ImageFormat.Jpeg);
                        break;
                    case ".png":
                        bm.Save(filepath, ImageFormat.Png);
                        break;
                    case ".gif":
                        bm.Save(filepath, ImageFormat.Gif);
                        break;
                }

                // Set Size and Position Objects
                Size s = new Size { Width = bm.Width, Height = bm.Height };
                Point p = new Point { X = 0, Y = 0 };
                t.Size = s;
                t.Position = p;
                t.Source = filepath;

                // Add to assets collection for access in UI
                assetList.Add(t);

                bm.Dispose();

                MessageBox.Show("Asset uploaded!", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                MessageBox.Show("A similarly named asset has already been uploaded!", "Reminder", MessageBoxButton.OK);
            }
        }
        #endregion  

        #region UPLOAD BACKGROUND IMAGE ASSET
        /// <summary>
        /// Upload Background Image Asset
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="finalDirPath"></param>
        private void UploadBackgroundImageAsset(string filename, string finalDirPath)
        {
            string extension = System.IO.Path.GetExtension(filename);
            string targetImagePath = this.GetTargetPath(System.IO.Path.GetFileNameWithoutExtension(filename), extension);

            Asset t = new Asset { ID = assetList.Count, Name = System.IO.Path.GetFileNameWithoutExtension(filename), Type = 0 };

            if (!assetList.Contains(t))
            {
                sourcePath = System.IO.Path.GetFullPath(filename);
                sourceFile = System.IO.Path.Combine(sourcePath, filename);
                destFile = System.IO.Path.Combine(finalDirPath, filename);

                string filepath = finalDirPath + @"\" + System.IO.Path.GetFileName(filename);

                System.Drawing.Bitmap bm = System.Drawing.Image.FromFile(filename) as System.Drawing.Bitmap;

                // Upload by extension
                switch (extension)
                {
                    case ".bmp":
                        bm.Save(filepath, ImageFormat.Bmp);
                        break;
                    case ".jpg":
                        bm.Save(filepath, ImageFormat.Jpeg);
                        break;
                    case ".png":
                        bm.Save(filepath, ImageFormat.Png);
                        break;
                    case ".gif":
                        bm.Save(filepath, ImageFormat.Gif);
                        break;
                }

                // Set Size and Position Objects
                Size s = new Size { Width = bm.Width, Height = bm.Height };
                Point p = new Point { X = 0, Y = 0 };
                t.Size = s;
                t.Position = p;
                t.Source = filepath;

                // Update Project XML file
                this.doc = new XMLDocManager();
                this.doc.AddSceneBackgroundNode(t);

                // Add to assets collection for access in UI
                assetList.Add(t);

                MainWindow.main.CanvasItem.Background = new ImageBrush(new BitmapImage(new Uri(filepath)));
                MainWindow.main.BtnCanvasBg.Tag = new ImageBrush(new BitmapImage(new Uri(filepath)));

                bm.Dispose();

                MessageBox.Show("Asset uploaded!", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                MessageBox.Show("A similarly named asset has already been uploaded!", "Reminder", MessageBoxButton.OK);
            }
        }
        #endregion  

        #region DROP IMAGE
        /// <summary>
        /// Upload Dropped Image Assets
        /// </summary>
        /// <param name="files"></param>
        public void DropImage(string[] files)
        {
            string finalDirPath = this.destPath + @"\" + Globals.Name;

            if (!Directory.Exists(finalDirPath))
            {
                Directory.CreateDirectory(finalDirPath);
            }

            foreach (string file in files)
            {
                if ((Path.GetExtension(file).ToLower() == ".jpg") || (Path.GetExtension(file).ToLower() == ".png") || (Path.GetExtension(file).ToLower() == ".bmp") || (Path.GetExtension(file).ToLower() == ".gif"))
                {
                    this.UploadImageAsset(file, finalDirPath);
                }

            }
        }
        #endregion

        #region ADD SOUND DIALOG
        /// <summary>
        /// Show sound file explorer dialog
        /// </summary>
        public void AddSoundDialog()
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "Sound files (*.wav;*.mp3)|*.wav;*.mp3";
                openFileDialog.Multiselect = false;
                openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyMusic);
                openFileDialog.Title = "Choose a Sound file";

                if (!Directory.Exists(destPath))
                {
                    Directory.CreateDirectory(destPath);
                }

                if (openFileDialog.ShowDialog() == true)
                {
                    filename = openFileDialog.FileName;
                    string targetImagePath = this.GetTargetPath(System.IO.Path.GetFileNameWithoutExtension(filename), System.IO.Path.GetExtension(filename));
                    


                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "File Openning Error", MessageBoxButton.OK);
            }
        }
        #endregion

        #region SAVE PROJECT DIALOG/METHODS
        /// <summary>
        /// Save the project
        /// </summary>
        public void SaveProjectDialog(){
            if (Globals.Init())
            {
                //SaveFileDialog saveFileDialog = new SaveFileDialog();
                //saveFileDialog.DefaultExt = ".scene";
                //saveFileDialog.Filter = "Mini2D Scene files (*.scene)|*.scene";
                //saveFileDialog.InitialDirectory = this.projectPath;
                //saveFileDialog.Title = "Save Project";
                //saveFileDialog.FileName = Globals.Name;

                //if (saveFileDialog.ShowDialog() == true)
                //{
                    string fpath = Directory.GetParent(@"../../").FullName + @"\projects\" + Globals.Name + ".xml";
                    this.scene.UpdateSceneObjects();
                    this.doc.SerializeToXML(fpath);
                    Globals.Saved = true;
                    this.CreateMessageBox("Project " + Globals.Name + " saved!", 1);
                //}
            }
            else { 
                this.CreateMessageBox("Create or Open a Project first", 0); 
            }
        }
        
        #endregion

        #region CLEAR ASSET LIST
        /// <summary>
        /// Clear the assets collections
        /// </summary>
        public void ClearAssetLists()
        {
            assetList.Clear();
            MainWindow.main.assetListCollection.Clear();
        }
        #endregion

        #region READ FILES FROM FOLDER
        /// <summary>
        /// Read all asset files from project asset folder and add to assetlist collection
        /// </summary>
        public void ReadFilesFromFolder()
        {
            this.ClearAssetLists();

            string directoryPath = this.destPath + @"\" + Globals.Name;

            try
            {
                if (Directory.Exists(directoryPath))
                {
                    DirectoryInfo dirInfo = new DirectoryInfo(directoryPath);
                    //string[] extensions = new[] { "*.bmp", "*.png", "*.jpg", "*.gif" };

                    FileInfo[] bmp_info = dirInfo.GetFiles("*.bmp", SearchOption.TopDirectoryOnly);
                    FileInfo[] png_info = dirInfo.GetFiles("*.png", SearchOption.TopDirectoryOnly);
                    FileInfo[] jpg_info = dirInfo.GetFiles("*.jpg", SearchOption.TopDirectoryOnly);
                    FileInfo[] gif_info = dirInfo.GetFiles("*.gif", SearchOption.TopDirectoryOnly);

                    FileInfo[] info_bmp_png = (FileInfo[])bmp_info.Concat<FileInfo>(png_info).ToArray();
                    FileInfo[] info_jpg_gif = (FileInfo[])jpg_info.Concat<FileInfo>(gif_info).ToArray();

                    FileInfo[] info_all = (FileInfo[])info_bmp_png.Concat<FileInfo>(info_jpg_gif).ToArray();

                    foreach (FileInfo f in info_all)
                    {
                        //Debug.dd("ReadFilesFromFolder - Source", directoryPath + @"\" + f.Name );
                        assetList.Add(new Asset { Name = System.IO.Path.GetFileNameWithoutExtension(f.Name), Source = directoryPath + @"\" + f.Name });
                    }

                    MainWindow.main.assetListCollection = assetList;
                }
                else
                {
                    MainWindow.main.assetListCollection = new ObservableCollection<Asset>();
                }

            }
            catch (Exception ex)
            {
                Debug.dd("ReadFilesFromFolder - Exception", ex.Message);
            }
        }
        #endregion

        #region GET TARGET PATH
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fname"></param>
        /// <returns></returns>
        private string GetTargetPath(string filename, string ext)
        {
            return Directory.GetParent(@"../../").FullName + @"\assets\" + filename + ext;
        }
        #endregion

        #region CREATE MESSAGE DIALOG BOX
        public void CreateMessageBox(string message, int mType) {
            switch (mType)
            {
                case 0:
                    MessageBox.Show(message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
                    break;
               case 1:
                    MessageBox.Show(message, "Success", MessageBoxButton.OK, MessageBoxImage.Information);
                    break;
                case 2:
                    MessageBox.Show(message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    break;
                case 3:
                    MessageBox.Show(message, "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    break;
                default:
                    MessageBox.Show(message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
                    break;
            }
           
        }
        #endregion

    }

    
}
