﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Media;

namespace mini2d_demo
{
    public class SceneObjectViewModel: Asset, Behaviours.IPlatform, Behaviours.IPlayer
    {
        #region Data Members

        /// <summary>
        /// The color of the rectangle.
        /// </summary>
        private Color color;

        private Color stroke;

        #endregion Data Members

        public bool isHoverable() { return false; }
        public bool isGround() { return false; }
        public bool isGrounded() { return false; }
        public bool isMovable() { return false; }
        public bool canBeMovedLeft() { return false; }
        public bool canBeMovedRight() { return false; }
        public bool canJump() { return false; }

        public SceneObjectViewModel() { }

        public SceneObjectViewModel(double x, double y, double width, double height, Color color, Color stroke)
        {
            this.Position = new Point { X = x, Y = y};
            this.Size = new Size { Width = width, Height = height };
            this.color = color;
            this.stroke = stroke;
        }

       
        /// <summary>
        /// The X coordinate of the location of the rectangle (in content coordinates).
        /// </summary>
        public double X
        {
            get
            {
                return Position.X;
            }
            set
            {
                if (Position.X == value)
                {
                    return;
                }

                position.X = value;

                OnPropertyChanged("X");
            }
        }

        /// <summary>
        /// The Y coordinate of the location of the rectangle (in content coordinates).
        /// </summary>
        public double Y
        {
            get
            {
                return Position.Y;
            }
            set
            {
                if (Position.Y == value)
                {
                    return;
                }

                position.Y = value;

                OnPropertyChanged("Y");
            }
        }

        /// <summary>
        /// The width of the rectangle (in content coordinates).
        /// </summary>
        public double Width
        {
            get
            {
                return Size.Width;
            }
            set
            {
                if (Size.Width == value)
                {
                    return;
                }

                Size.Width = value;

                OnPropertyChanged("Width");
            }
        }

        /// <summary>
        /// The height of the rectangle (in content coordinates).
        /// </summary>
        public double Height
        {
            get
            {
                return Size.Height;
            }
            set
            {
                if (Size.Height == value)
                {
                    return;
                }

                Size.Height = value;

                OnPropertyChanged("Height");
            }
        }

        /// <summary>
        /// The color of the item.
        /// </summary>
        public Color Color
        {
            get
            {
                return color;
            }
            set
            {
                if (color == value)
                {
                    return;
                }

                color = value;

                OnPropertyChanged("Color");
            }
        }

        /// <summary>
        /// The color of the stroke.
        /// </summary>
        public Color Stroke
        {
            get
            {
                return stroke;
            }
            set
            {
                if (stroke == value)
                {
                    return;
                }

                stroke = value;

                OnPropertyChanged("Stroke");
            }
        }   


    }
}
