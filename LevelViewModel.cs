﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mini2d_demo
{
    public class LevelViewModel: INotifyPropertyChanged
    {
        public string name;
        public string order;
        public int width;
        public int height;

        public LevelViewModel() { }

        public LevelViewModel(string name, string order, int width, int height) 
        {
            this.name = name;
            this.order = order;
            this.width = width;
            this.height = height;
        }

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                if (name == value)
                {
                    return;
                }

                name = value;

                OnPropertyChanged("name");
            }
        }

        public string Order 
        {
            get
            {
                return order;
            }
            set
            {
                if (order == value)
                {
                    return;
                }

                order = value;

                OnPropertyChanged("order");
            }
        }

        public int Width 
        {
            get
            {
                return width;
            }
            set
            {
                if (width == value)
                {
                    return;
                }

                width = value;

                OnPropertyChanged("width");
            }
        }

        public int Height 
        {
            get
            {
                return height;
            }
            set
            {
                if (height == value)
                {
                    return;
                }

                height = value;

                OnPropertyChanged("height");
            }
        }

        #region INotifyPropertyChanged Members

        /// <summary>
        /// Raises the 'PropertyChanged' event when the value of a property of the view model has changed.
        /// </summary>
        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        /// <summary>
        /// 'PropertyChanged' event that is raised when the value of a property of the view model has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

    }
}
