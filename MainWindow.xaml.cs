﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using MahApps.Metro.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;
//using Xceed.Wpf.Toolkit;

namespace mini2d_demo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow, INotifyPropertyChanged
    {

        #region PROPERTIES
        /// <summary>
        /// Objects
        /// </summary>
        UIManager ui;
        Scene scene;
        ViewModel vm;

        /// <summary>
        /// Collections
        /// </summary>
        private ObservableCollection<Asset> assetList = new ObservableCollection<Asset>();
        private ObservableCollection<SceneObjectViewModel> sceneList = new ObservableCollection<SceneObjectViewModel>();

        /// <summary>
        /// INotifyPropertyChanged 
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        /// <summary>
        /// Bools
        /// </summary>
        private bool mouseDown = false;

        /// <summary>
        /// Cursor variable
        /// </summary>
        private Cursor customCursor = null;

        /// <summary>
        /// Starting Point
        /// </summary>
        private System.Windows.Point _startPoint;

        /// <summary>
        /// Are we dragging in the MainWindow.main.CanvasItem
        /// </summary>
        private bool draggingToCanvas = false;
        
        #endregion

        #region CONSTRUCTOR
        public MainWindow()
        {
            InitializeComponent(); 
            main = this;
            Globals.Canvas = canvas;
            ui = new UIManager();
            ui.Run();
            //ViewModel vm = new ViewModel();

            listAssets.ItemsSource = this.assetListCollection;
            //sceneItemList.ItemsSource = this.sceneCollection;
            sceneCollection = new ObservableCollection<SceneObjectViewModel>();

            DataContext = this;
            //canvasItemsControl.ItemsSource = vm.SceneObjects;
            //dragSelectBoxIC.ItemsSource = vm.DragSelectBox;

            scene = new Scene();
            scene.Set();
            
            canvasBackgroundColorPicker.IsOpen = false;
            canvasBackgroundColorPicker.SelectedColor = Colors.Black;
            canvasBackgroundColorPicker.ShowAdvancedButton = true;
           
        }

       
        #endregion

        /**************************************** ASSET LIST IMAGE MOUSE EVENTS *****************************************/

        #region LIST ASSETS GIVE FEEDBACK
        private void listAssets_GiveFeedback_1(object sender, GiveFeedbackEventArgs e)
        {
            e.UseDefaultCursors = e.Effects != DragDropEffects.Copy;

            if (e.Effects == DragDropEffects.Copy)
            {
                if (customCursor == null)
                    customCursor = CursorHelper.CreateCursor(e.Source as UIElement);

                if (customCursor != null)
                {
                    e.UseDefaultCursors = false;
                    Mouse.SetCursor(customCursor);
                }
            }
            else
            {
                e.UseDefaultCursors = true;
            }

            e.Handled = true;
        }
        #endregion

        #region IMAGE PREVIEW MOUSE LEFT BTN DOWN
        private void Image_PreviewMouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            // The initial mouse position
            _startPoint = e.GetPosition(null);
            mouseDown = true;
        }
        #endregion

        #region IMAGE PREVIEW MOUSE MOVE
        private void Image_PreviewMouseMove_1(object sender, MouseEventArgs e)
        {
            if (mouseDown)
            {
                // We are indeed dragging
                draggingToCanvas = true;

                // Get the current mouse position
                System.Windows.Point mousePos = e.GetPosition(null);
                Vector diff = _startPoint - mousePos;

                if (e.LeftButton == MouseButtonState.Pressed &&
                    (Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance ||
                    Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance))
                {
                    // Get the dragged ListBoxItem

                    var img = sender as System.Windows.Controls.Image;

                    if (customCursor == null)
                    {
                        customCursor = CursorHelper.CreateCursor(e.Source as UIElement);
                    }

                    if (customCursor != null)
                    {
                        Mouse.SetCursor(customCursor);
                    }

                    try
                    {
                        DataObject dragData = new DataObject(DataFormats.FileDrop, img);

                        //if the data is not null then start the drag drop operation
                        if (dragData != null)
                            DragDrop.DoDragDrop(img, dragData, DragDropEffects.Copy);

                    }
                    catch (Exception ex)
                    {
                        Debug.dd("listAssets_PreviewMouseMove_1", ex.Message);
                        MessageBox.Show("Upload failed!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
        }
        #endregion

        #region IMAGE PREVIEW MOUSE LEFT BUTTON UP
        private void Image_PreviewMouseLeftButtonUp_1(object sender, MouseButtonEventArgs e)
        {
            _startPoint = new System.Windows.Point();
            mouseDown = false;
            draggingToCanvas = false;
            customCursor = null;
        }
        #endregion

        /**************************************** ASSET LIST DRAG-DROP METHODS ******************************************/

        #region ASSET LIST DRAG-ENTER EVENTS
        public void listAssets_DragEnter(object sender, DragEventArgs e)
        {

            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effects = DragDropEffects.Copy;
            }
            else
            {
                e.Effects = DragDropEffects.None;
            }
        }
        #endregion

        #region ASSET LIST DRAG-LEAVE
        public void listAssets_DragLeave(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.All;
        }
        #endregion

        #region ASSET LIST DRAG-OVER
        public void listAssets_DragOver(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.All;
        }
        #endregion

        #region ASSET LIST DROP
        private void listAssets_Drop_1(object sender, DragEventArgs e)
        {
            if (!draggingToCanvas)
            {
                if (Globals.Init())
                {
                    string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                    ui.DropImage(files);
                }
                else
                {
                    MessageBox.Show("Create a project first!", "Sorry", MessageBoxButton.OK, MessageBoxImage.Information);
                }

            }
        }
        #endregion

        /**************************************** INTERNAL VARS *********************************************************/

        #region INTERNAL VARS
        internal static MainWindow main;
        private bool clicked;
        private FrameworkElement selectedElement;

        internal string title {
            get { return Title; }
            set { Dispatcher.Invoke(new Action(() => { Title = value; })); }
        }

        internal Canvas CanvasItem
        {
            get { return canvas; }
            set { Dispatcher.Invoke(new Action(() => { canvas = value; RaisePropertyChanged("canvas"); })); }
        }

        //internal ItemsControl ic
        //{
        //    get { return canvasItemsControl; }
        //    set { Dispatcher.Invoke(new Action(() => { canvasItemsControl = value; RaisePropertyChanged("canvasItemsControl"); })); }
        //}

        internal TextBox AssetWidth
        {
            get { return assetWidth; }
            set { Dispatcher.Invoke(new Action(() => { assetWidth = value; RaisePropertyChanged("assetWidth"); })); }
        }

        internal TextBox AssetHeight
        {
            get { return assetHeight; }
            set { Dispatcher.Invoke(new Action(() => { assetHeight = value; RaisePropertyChanged("assetHeight"); })); }
        }
       
        internal ContextMenu SceneObjectmenu
        {
            get { return SceneObjectmenu; }
            set { Dispatcher.Invoke(new Action(() => { SceneObjectmenu = value; RaisePropertyChanged("SceneObjectMenu"); })); }
        }

        internal Xceed.Wpf.Toolkit.ColorPicker CanvasColorPicker
        {
            get { return canvasBackgroundColorPicker; }
            set { Dispatcher.Invoke(new Action(() => { canvasBackgroundColorPicker = value; RaisePropertyChanged("canvasBackgroundColorPicker"); })); }
        }

        internal Button BtnCanvasBg
        {
            get { return setCanvasBg; }
            set { Dispatcher.Invoke(new Action(() => { setCanvasBg = value; RaisePropertyChanged("setCanvasBg"); })); }
        }

        internal ScrollViewer scroller
        {
            get { return canvasScroller; }
            set { Dispatcher.Invoke(new Action(() => { canvasScroller = value; RaisePropertyChanged("canvasScroller"); })); }
        }

        internal Grid CenterGrid
        {
            get { return centerGrid; }
            set { Dispatcher.Invoke(new Action(() => { centerGrid = value; RaisePropertyChanged("centerGrid"); })); }
        }

        internal Grid SceneGrid
        {
            get { return sceneGrid; }
            set { Dispatcher.Invoke(new Action(() => { sceneGrid = value; RaisePropertyChanged("centerGrid"); })); }
        }

        internal ObservableCollection<Asset> assetListCollection
        {
            get { return assetList; }
            set { Dispatcher.Invoke(new Action(() => { assetList = value; RaisePropertyChanged("assetList"); })); }
        }

        internal ObservableCollection<SceneObjectViewModel> sceneCollection
        {
            get { return sceneList; }
            set { Dispatcher.Invoke(new Action(() => { sceneList = value; RaisePropertyChanged("sceneList"); })); }
        }

        #endregion

        /**************************************** MAIN MENU COMMANDS *********************************************************/

        #region Menu Commands

        #region CUT COMMAND
        private void CommandBinding_CanExecute_1(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
        
        private void CommandBinding_Executed_1(object sender, ExecutedRoutedEventArgs e)
        {

        }
        #endregion

        #region Paste
        private void CommandBinding_CanExecute_2(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
        

        private void CommandBinding_Executed_2(object sender, ExecutedRoutedEventArgs e)
        {

        }
        #endregion

        #region Copy
        private void CommandBinding_CanExecute_3(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
        private void CommandBinding_Executed_3(object sender, ExecutedRoutedEventArgs e)
        {
            
        }
        #endregion

        #region Open New Project
        private void CommandBinding_CanExecute_4(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
        private void CommandBinding_Executed_4(object sender, ExecutedRoutedEventArgs e)
        {
            if (Globals.Init() && !Globals.Saved) { MessageBox.Show("Save the current project first!", "Save project", MessageBoxButton.OK, MessageBoxImage.Information); }
            else { ui.OpenProjectDialog(); }
        }
        #endregion

        #region Save File
        private void CommandBinding_CanExecute_5(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CommandBinding_Executed_5(object sender, ExecutedRoutedEventArgs e)
        {
           ui.SaveProjectDialog(); 
        }
        #endregion

        #region Close
        private void CommandBinding_CanExecute_6(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CommandBinding_Executed_6(object sender, ExecutedRoutedEventArgs e)
        {
            this.Close();
        }
        #endregion

        #region Start New Project
        private void CommandBinding_CanExecute_7(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CommandBinding_Executed_7(object sender, ExecutedRoutedEventArgs e)
        {
            if (Globals.Saved) { MessageBox.Show("Save the current project first!", "Save project", MessageBoxButton.OK, MessageBoxImage.Information); }
            else { ui.StartNewProjectDialog(); }
        }
        #endregion

        #region Undo
        private void CommandBinding_CanExecute_8(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CommandBinding_Executed_8(object sender, ExecutedRoutedEventArgs e)
        {

        }
        #endregion

        #region Redo
        private void CommandBinding_CanExecute_9(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
        private void CommandBinding_Executed_9(object sender, ExecutedRoutedEventArgs e)
        {

        }
        #endregion

        #region Delete
        private void CommandBinding_CanExecute_10(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
        private void CommandBinding_Executed_10(object sender, ExecutedRoutedEventArgs e)
        {

        }
        #endregion

        #endregion

        /**************************************** CONTEXT MENU MOUSE EVENTS *****************************************/

        #region CLEAR SCENE
        public void clearScene_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                vm.SceneObjects.Clear();
                //this.sceneCollection = new ObservableCollection<SceneObjectViewModel>();
            }
            catch (Exception ex) {
                Debug.dd("Clear Scene", ex.Message);
            }
        }
        #endregion

        #region DELETE SCENE OBJECT
        public void DeleteSceneObject_Click(object sender, RoutedEventArgs e)
        {
           
            MenuItem m = sender as MenuItem;
            if (m != null)
            {
                ContextMenu cm = m.CommandParameter as ContextMenu;
                if (cm != null)
                {
                    var target = (System.Windows.Shapes.Rectangle)ContextMenuService.GetPlacementTarget(LogicalTreeHelper.GetParent(m));

                    if (target != null)
                    {
                        scene.DeleteObject(Int32.Parse(target.Uid));
                        canvas.Children.Remove(target);
                    }
                }
            }

        }
        #endregion

        /**************************************** CANVAS MOUSE EVENTS ***************************************************/

        #region MOUSE EVENTS
        #region MOUSE ENTER
        private void canvas_MouseEnter_1(object sender, MouseEventArgs e)
        {
            scene.CanvasMouseEnter(sender as Canvas, e);
        }
        #endregion

        #region CANVAS LEFT BTN DOWN
        private void canvas_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            scene.CanvasMouseLeftBtnDown(sender as Canvas, e);
            if (e.Source.GetType() == typeof(Canvas))
            {
                this.EnableCanvasUIElements();
                this.DisableUIElements();
            }
            else if (e.Source.GetType() == typeof(System.Windows.Shapes.Rectangle))
            {
                this.EnableUIElements();
                this.DisableCanvasUIElements();
            }
        }
        #endregion

        #region CANVAS MOUSE LEFT BTN UP
        private void canvas_MouseLeftButtonUp_1(object sender, MouseButtonEventArgs e)
        {
            scene.CanvasMouseLeftBtnUp(sender, e);
        }
        #endregion

        #region CANVAS PREVIEW MOUSE LEFT BTN DOWN
        private void canvas_PreviewMouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            scene.PreviewCanvasMouseLeftBtnDown(sender as Canvas, e);
        }
        #endregion

        #region CANVAS PREVIEW MOUSE LEFT BTN UP
        private void canvas_PreviewMouseLeftButtonUp_1(object sender, MouseButtonEventArgs e)
        {
            scene.DragFinishedMouseHandler(sender as Canvas, e);
        }
        #endregion

        #region CANVAS MOUSE MOVE
        private void canvas_MouseMove_1(object sender, MouseEventArgs e)
        {
            scene.CanvasMouseMove(sender as Canvas, e);
        }
        #endregion

        #region CANVAS MOUSE WHEEL
        private void canvas_MouseWheel_1(object sender, MouseWheelEventArgs e)
        {
            scene.Zoom(sender as Canvas, e);
        }
        #endregion

        #region CANVAS MOUSE RIGHT BTN DOWN
        private void canvas_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            scene.ShowContextMenu(sender, e);
        }
        #endregion

        #region CANVAS MOUSE RIGHT BTN UP
        private void canvas_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            //
        }
        #endregion

        #region CANVAS MOUSE LEAVE
        private void canvas_MouseLeave_1(object sender, MouseEventArgs e)
        {
            scene.CanvasMouseLeave(sender as Canvas, e);
        }
        #endregion

        #region CANVAS DROP
        private void canvas_Drop_1(object sender, DragEventArgs e)
        {
            //get the data from the arguments. 
            //We specify typeof string since we know our data type

            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                try
                {
                    System.Windows.Controls.Image item = e.Data.GetData(DataFormats.FileDrop) as System.Windows.Controls.Image;
                    string targetImagePath = item.Source.ToString();
                    BitmapImage bitmap = new BitmapImage();
                    bitmap.BeginInit();
                    bitmap.UriSource = new Uri(targetImagePath);
                    bitmap.EndInit();

                    // Add asset to scene
                    scene.DropOnCanvas(sender as Canvas, e.GetPosition(sender as Canvas), bitmap);
                }
                catch (Exception ex)
                {
                    Debug.dd("MainWindow.main.CanvasItem_Drop_1", ex.Message);
                    MessageBox.Show("Item mouse drop to scene failed!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
        #endregion

        #region CANVAS DRAG-ENTER
        private void canvas_DragEnter_1(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effects = DragDropEffects.Copy;
            }
            else
            {
                e.Effects = DragDropEffects.None;
            }
        }
        #endregion

        #region CANVAS DRAG-LEAVE
        private void canvas_DragLeave_1(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.Copy;
            customCursor = null;
        }
        #endregion

        #region CANVAS DRAG-OVER
        private void canvas_DragOver_1(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.Copy;
            customCursor = null;
        }
        #endregion
        #endregion

        /// <summary>
        /// Handle the user dragging the rectangle.
        /// </summary>
        private void Thumb_DragDelta(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
        {
            Thumb thumb = (Thumb)sender;
            SceneObjectViewModel sceneObject = (SceneObjectViewModel)thumb.DataContext;

            //
            // Update the the position of the rectangle in the view-model.
            //
 
            sceneObject.X += e.HorizontalChange;
            sceneObject.Y += e.VerticalChange;
            sceneObject.Stroke = System.Windows.Media.Colors.Blue;
        }

        private void Thumb_DragCompleted(object sender, System.Windows.Controls.Primitives.DragCompletedEventArgs e)
        {
            Thumb thumb = (Thumb)sender;
            SceneObjectViewModel sceneObject = (SceneObjectViewModel)thumb.DataContext;
            sceneObject.Stroke = System.Windows.Media.Colors.Transparent;
        }

        /**************************************** CANVAS SCROLLER MOUSE EVENTS ******************************************/

        #region CANVAS SCROLLER MOUSE EVENTS
        private void ScrollViewer_PreviewMouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            scene.ScrollerMouseLeftBtnDown(sender, e);
        }

        private void ScrollViewer_PreviewMouseLeftButtonUp_1(object sender, MouseButtonEventArgs e)
        {
            scene.ScrollerMouseLeftBtnUp(sender, e);
        }

        private void ScrollViewer_PreviewMouseMove_1(object sender, MouseEventArgs e)
        {
            scene.ScrollerMouseMove(sender, e);
        }
        #endregion

        /**************************************** SCENE ITEM LISTVIEW MOUSE EVENTS **************************************/

        #region SCENE ITEM LISTVIEW MOUSE ENTER
        private void Image_MouseEnter_1(object sender, MouseEventArgs e)
        {
            //sceneItemList.CaptureMouse();
        }
        #endregion 

        #region SCENE ITEM LISTVIEW PREVIEW MOUSE LEFT BTN DOWN
        private void Image_PreviewMouseLeftButtonDown_2(object sender, MouseButtonEventArgs e)
        {
            System.Windows.Controls.Image img = sender as System.Windows.Controls.Image;
        }
        #endregion

        #region SCENE ITEM LISTVIEW PREVIEW MOUSE LEFT BTN UP
        private void Image_PreviewMouseLeftButtonUp_2(object sender, MouseButtonEventArgs e)
        {
            //sceneItemList.ReleaseMouseCapture();
        }

        #endregion 

        /**************************************** LEFT SIDEBAR EXPANDER EVENTS **************************************/

        #region LEFT SIDEBAR EXPANDER COLLASPED-EXPANDED
        private void Expander_Collapsed(object sender, RoutedEventArgs e)
        {
            //
        }

        private void Expander_Expanded(object sender, RoutedEventArgs e)
        {
            //
        }
        #endregion

        /**************************************** SOME UI EVENTS **************************************/

        #region COLOR PICKER SELECTED COLOR CHANGE
        private void canvasBackgroundColorPicker_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<System.Windows.Media.Color?> e)
        {
            Xceed.Wpf.Toolkit.ColorPicker p = sender as Xceed.Wpf.Toolkit.ColorPicker;
            if (Globals.Init() && Globals.HasLevels()) { scene.SetBackground((System.Windows.Media.Color)p.SelectedColor); }
            RaisePropertyChanged("color_picker");
        }
        #endregion

        #region TOGGLE SIDEBAR
        private void toggleSidebar_Click(object sender, RoutedEventArgs e)
        {
            MainGrid.ColumnDefinitions.Clear();
            if (!clicked)
            {
                clicked = true;
                MainGrid.ColumnDefinitions.Clear();
                leftScroller.Visibility = Visibility.Collapsed;
                ColumnDefinition c1 = new ColumnDefinition();
                ColumnDefinition c2 = new ColumnDefinition();
                ColumnDefinition c3 = new ColumnDefinition();
                c1.Width = new GridLength(0, GridUnitType.Star);
                c2.Width = new GridLength(100, GridUnitType.Star);
                c3.Width = new GridLength(0, GridUnitType.Star);
                MainGrid.ColumnDefinitions.Add(c1);
                MainGrid.ColumnDefinitions.Add(c2);
                MainGrid.ColumnDefinitions.Add(c3);
            }
            else {
                clicked = false;
                MainGrid.ColumnDefinitions.Clear();
                leftScroller.Visibility = Visibility.Visible;
                ColumnDefinition c1 = new ColumnDefinition();
                ColumnDefinition c2 = new ColumnDefinition();
                ColumnDefinition c3 = new ColumnDefinition();
                c1.Width = new GridLength(1, GridUnitType.Star);
                c2.Width = new GridLength(2.5, GridUnitType.Star);
                c3.Width = new GridLength(1, GridUnitType.Star);
                MainGrid.ColumnDefinitions.Add(c1);
                MainGrid.ColumnDefinitions.Add(c2);
                MainGrid.ColumnDefinitions.Add(c3);
            }
        }
        #endregion

        #region SET CANVAS CLICK
        private void setCanvasBg_Click(object sender, RoutedEventArgs e)
        {
            ui.AddBackgroundImageDialog();
        }
        #endregion

        /**************************************** GENERAL METHODS *******************************************************/

        #region GENERAL METHODS
        /// <summary>
        /// Raise Property Changed Event
        /// </summary>
        /// <param name="propName"></param>
        public void RaisePropertyChanged(string propName)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }

        private void menu_Click(object sender, RoutedEventArgs e)
        {
            ui.RunMenu(sender, e);
        }

        private void MainWin_Closing_1(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.Shutdown();
        }
        #endregion

        /**************************************** HELPER METHODS ********************************************************/

        #region GET OBJECT DATA FROM POINT IN LISTBOX
        //gets the object for the element selected (from the point) in the listbox (source)
        private static object GetObjectDataFromPoint(ListBox source, System.Windows.Point point)
        {
            UIElement element = source.InputHitTest(point) as UIElement;
            if (element != null)
            {
                //get the object from the element
                object data = DependencyProperty.UnsetValue;
                while (data == DependencyProperty.UnsetValue)
                {
                    // try to get the object value for the corresponding element
                    data = source.ItemContainerGenerator.ItemFromContainer(element);

                    //get the parent and we will iterate again
                    if (data == DependencyProperty.UnsetValue)
                        element = VisualTreeHelper.GetParent(element) as UIElement;

                    //if we reach the actual listbox then we must break to avoid an infinite loop
                    if (element == source)
                        return null;
                }

                //return the data that we fetched only if it is not Unset value, 
                //which would mean that we did not find the data
                if (data != DependencyProperty.UnsetValue)
                    return data;
            }

            return null;
        }
        #endregion

        #region ENABLE SCENE OBJECT UI ELEMENTS
        private void EnableUIElements()
        {
            this.objectType.IsEnabled = true;
            this.pAwardable.IsEnabled = true;
            this.pDeductable.IsEnabled = true;
            this.keyMoveLeft.IsEnabled = true;
            this.keyMoveRight.IsEnabled = true;
            this.keyShoot.IsEnabled = true;
        }
        #endregion

        #region DISABLE SCENE OBJECT UI ELEMENTS
        private void DisableUIElements() {
            this.objectType.IsEnabled = false;
            this.pAwardable.IsEnabled = false;
            this.pDeductable.IsEnabled = false;
            this.keyMoveLeft.IsEnabled = false;
            this.keyMoveRight.IsEnabled = false;
            this.keyShoot.IsEnabled = false;
        }
        #endregion

        #region ENABLE CANVAS UI ELEMENTS
        private void EnableCanvasUIElements()
        {
            this.CanvasColorPicker.IsEnabled = true;
            this.BtnCanvasBg.IsEnabled = true;
        }
        #endregion

        #region DISABLE CANVAS UI ELEMENTS
        public void DisableCanvasUIElements()
        {
            this.CanvasColorPicker.IsEnabled = false;
            this.BtnCanvasBg.IsEnabled = false;
        }
        #endregion

        private void Rectangle_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            selectedElement = sender as FrameworkElement;
            (sender as FrameworkElement).ContextMenu = MainWindow.main.FindResource("SceneObjectMenu") as ContextMenu;
        }

        private void Rectangle_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            selectedElement = sender as FrameworkElement;
            this.EnableCanvasUIElements();
            this.DisableCanvasUIElements();
        }

        System.Windows.Shapes.Rectangle copied;

        #region COPY FUNCTION
        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem m = sender as MenuItem;
            if (m != null)
            {
                ContextMenu cm = m.CommandParameter as ContextMenu;
                if (cm != null)
                {
                    var target = (System.Windows.Shapes.Rectangle)ContextMenuService.GetPlacementTarget(LogicalTreeHelper.GetParent(m));

                    if (target != null)
                    {
                        copied = target as System.Windows.Shapes.Rectangle;
                        Clipboard.SetDataObject(copied.Fill, false);
                        
                    }
                }
            }
           

        }
        #endregion

        #region PASTE FUNCTION
        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            IDataObject clipboardData = Clipboard.GetDataObject();

            if (clipboardData != null)
            {

                ImageBrush imgbrush = (ImageBrush)clipboardData.GetData("System.Windows.Media.Brush", true);
                try
                {
                    System.Windows.Shapes.Rectangle copiedObject = new System.Windows.Shapes.Rectangle { 
                        Uid = sceneCollection.Count.ToString(),
                        Width = copied.Width,
                        Height = copied.Height,
                        Fill = imgbrush,
                    };

                    double newLeft = Canvas.GetLeft(copied) + copied.Width;
                    double newTop = Canvas.GetTop(copied) + copied.Height/2;
                    Canvas.SetLeft(copiedObject, newLeft);
                    Canvas.SetTop(copiedObject, newTop);
                    canvas.Children.Add(copiedObject);
                    Console.WriteLine("Clipboard copied to scene");
                    Clipboard.Flush();
                }
                finally
                {
                    //DeleteObject(hBitmap);
                }
               
            }
        }
        #endregion
    }

}
