﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Media;

namespace mini2d_demo
{
    public class Asset : INotifyPropertyChanged
    {

        public int id;
        public string name;
        public int type;
        public string source;

        public Point position;
        public Size size;
         
        public int ID
        {
            get
            {
                return id;
            }
            set
            {
                if (id == value)
                {
                    return;
                }

                id = value;

                OnPropertyChanged("id");
            }
        }

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                if (name == value)
                {
                    return;
                }

                name = value;

                OnPropertyChanged("name");
            }
        }

        public string Source
        {
            get
            {
                return source;
            }
            set
            {
                if (source == value)
                {
                    return;
                }

                source = value;

                OnPropertyChanged("source");
            }
        }

        public int Type
        {
            get
            {
                return type;
            }
            set
            {
                if (type == value)
                {
                    return;
                }

                type = value;

                OnPropertyChanged("type");
            }
        }

        public Size Size 
        {
            get
            {
                return size;
            }
            set
            {
                if (size == value)
                {
                    return;
                }

                size = value;

                OnPropertyChanged("size");
            }
        }

        public Point Position 
        {
            get
            {
                return position;
            }
            set
            {
                if (position == value)
                {
                    return;
                }

                position = value;

                OnPropertyChanged("position");
            }  
        }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        /// <summary>
        /// Raise Property Changed Event
        /// </summary>
        /// <param name="propName"></param>
        public void OnPropertyChanged(string propName)
        {
            try
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }


    }
}
