﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mini2d_demo.Behaviours
{
    interface IPlayer
    {
        bool canBeMovedLeft();

        bool canBeMovedRight();

        bool canJump();

    }
}
