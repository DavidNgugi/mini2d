﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mini2d_demo
{
    interface IScene : INotifyPropertyChanged
    {
        void RaisePropertyChanged(string propName);
    }
}
