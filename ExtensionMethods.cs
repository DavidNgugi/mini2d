﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mini2d_demo
{
    public static class ExtensionMethods
    {
        public static IEnumerable<FileInfo> GetFilesByExtension(this DirectoryInfo dirInfo, params string[] extensions)
        {
            if (extensions == null)
            {
                throw new ArgumentNullException("extensions");
            }

            IEnumerable<FileInfo> files = Enumerable.Empty<FileInfo>();

            foreach (string ext in extensions)
            {
                files = files.Concat(dirInfo.GetFiles(ext));
            }

            return extensions.SelectMany(dirInfo.GetFiles);
        }
    }
}
