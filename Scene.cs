﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Input;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Data;
using System.Windows.Controls.Primitives;

namespace mini2d_demo
{
    class Scene : FrameworkElement, IScene
    {
        #region PROPERTIES

        ViewModel vm;
        //AdornerLayer aLayer;

        /// <summary>
        /// Zoom
        /// </summary>
        private Double zoomMax = 5;
        private Double zoomMin = 0.5;
        private Double zoomSpeed = 0.001;
        private Double zoom = 1;

        /// <summary>
        /// Bools
        /// </summary>
        private bool isMouseDragging = false;
        private bool isMouseDown;
        bool _isDown;
        bool _isDragging;
        bool selected = false;

        //UIElement selectedElement = null;
        Rectangle selectedElement = null;

        //Point p;
        Point _startPoint;
        private double _originalLeft;
        private double _originalTop;

        /// <summary>
        /// Points
        /// </summary>
        private Point anchorPoint = new Point();
        private Point scrollMousePoint = new Point();

        private double hOff = 1;
        private double vOff = 1;

        /// <summary>
        /// 
        /// </summary>
        ImageBrush imgbrush;

        /// <summary>
        /// 
        /// </summary>
        //Rectangle selectBox;

        /// <summary>
        /// 
        /// </summary>
        Rectangle dragSelectBox;

        /// <summary>
        /// 
        /// </summary>
        Label lblWidth;

        /// <summary>
        /// 
        /// </summary>
        Label lblHeight;

        /// <summary>
        /// Collections
        /// </summary>
        private ObservableCollection<SceneObjectViewModel> sceneObjects = new ObservableCollection<SceneObjectViewModel>();

        /// <summary>
        /// BitmapImage object
        /// </summary>
        BitmapImage bitmap = new BitmapImage();

        /// <summary>
        /// Canvas Background
        /// </summary>
        public Brush Background { get { return Globals.Canvas.Background; } set { Globals.Canvas.Background = value; } }

        private Canvas ctx;

        private ScrollViewer sv;

        private Point mousePos = new Point();

        /// <summary>
        /// INotifyPropertyChanged 
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        private int originalZIndex;

        //List<MenuItem> mC = new List<MenuItem>();
        //List<MenuItem> mR = new List<MenuItem>();
        #endregion

        #region CONSTRUCTOR
        public Scene() {
            vm = new ViewModel();
            this.ctx = Globals.Canvas;
            this.sv = MainWindow.main.scroller;
            MainWindow.main.sceneCollection = this.sceneObjects;
            //this.AddSelectBox();
        }
        #endregion

        #region SETUP
        public void Set()
        {
            MainWindow.main.sceneCollection = sceneObjects;
        }
        #endregion

        #region ADD BOXES TO CANVAS
        /// <summary>
        /// Add pre-built objects to Canvas
        /// </summary>
        public void AddSelectBox()
        {
            try{
                if (this.ctx.Children.IndexOf(dragSelectBox) > 0)
                {
                    this.ctx.Children.Remove(dragSelectBox);
                    this.ctx.Children.Remove(lblWidth);
                    this.ctx.Children.Remove(lblHeight);
                }

                #region Drag Box
                dragSelectBox = new Rectangle();
                dragSelectBox.Name = "dragSelectBox";
                dragSelectBox.Fill = Brushes.AliceBlue;
                dragSelectBox.Opacity = 0.5;
                dragSelectBox.Stroke = Brushes.DarkBlue;
                dragSelectBox.Visibility = Visibility.Collapsed;
                dragSelectBox.SetValue(Canvas.ZIndexProperty, 1000);
                #endregion

                #region Width Label
                lblWidth = new Label();
                lblWidth.Name = "lblWidth";
                lblWidth.Foreground = Brushes.Red;
                lblWidth.Visibility = Visibility.Collapsed;
                lblWidth.SetValue(Canvas.ZIndexProperty, 1000);
                #endregion

                #region Height Label
                lblHeight = new Label();
                lblHeight.Name = "lblHeight";
                lblHeight.Foreground = Brushes.Red;
                lblHeight.Visibility = Visibility.Collapsed;
                lblHeight.SetValue(Canvas.ZIndexProperty, 1000);
                #endregion

                this.ctx.Children.Add(dragSelectBox);
                this.ctx.Children.Add(lblWidth);
                this.ctx.Children.Add(lblHeight);
                Debug.dd("CANVAS EXISTS AND WE'VE ADDED A BOX", this.ctx.Children.IndexOf(dragSelectBox));

            }catch(Exception e){
                Debug.dd("Error adding Boxes", e.Message);
            }
        }
        #endregion

        #region READ AND DRAW TO SCENE
        /// <summary>
        /// Draw from Project file
        /// </summary>
        public void ReadandDraw()
        {
            // Get scene data from project file

            // Render to scene

            // Set scene item lists
        }
        #endregion

        #region SET IMAGE BRUSH
        /// <summary>
        /// Set Image Brush from a image source path
        /// </summary>
        /// <returns></returns>
        private ImageBrush setImageBrush(string path)
        {
            bitmap = new BitmapImage();
            imgbrush = new ImageBrush();
            bitmap.BeginInit();
            bitmap.UriSource = new Uri(@"" + path);
            bitmap.EndInit();
            imgbrush.ImageSource = bitmap;

            return imgbrush;
        }
        #endregion

        #region SET IMAGE SOURCE
        /// <summary>
        /// Set Image Brush from a image source path
        /// </summary>
        /// <returns></returns>
        private ImageSource setImageSource(string path)
        {
            bitmap = new BitmapImage();
            imgbrush = new ImageBrush();
            bitmap.BeginInit();
            bitmap.UriSource = new Uri(@"" + path);
            bitmap.EndInit();
            imgbrush.ImageSource = bitmap;

            return imgbrush.ImageSource;
        }
        #endregion

        #region CREATE NEW SCENE AND LEVEL
        public void Create(LevelViewModel level)
        {
            vm.Levels.Add(level);
            //MainWindow.main.levels.ItemsSource = vm.Levels;
        }
        #endregion

        #region CREATE SCENE
        public void CreateScene(ObservableCollection<LevelViewModel> levelObject, ObservableCollection<SceneObjectViewModel> scene_objects, string bgImagePath, string bgColor)
        {
            this.ctx.IsEnabled = true;
            this.ctx.Visibility = Visibility.Visible;

            if (bgImagePath != string.Empty) { this.ctx.Background = setImageBrush(bgImagePath); }
            else if (bgImagePath == string.Empty && bgColor != string.Empty) { this.ctx.Background = new SolidColorBrush(FromName(bgColor)); }

            this.sceneObjects.Clear();
            this.ctx.Children.Clear();

            AddSelectBox();

            try
            {
                foreach (SceneObjectViewModel so in scene_objects)
                {
                    Rectangle img = new Rectangle
                    {
                        Fill = setImageBrush(so.Source),
                        Uid = so.ID.ToString(),
                        Width = so.Size.Width,
                        Height = so.Size.Height,
                        IsHitTestVisible = true,
                    };

                    img.Stretch = Stretch.Fill;
                    //img.MouseLeftButtonDown += sceneObject_MouseLeftBtnDown;
                    //img.MouseLeftButtonUp += sceneObject_MouseLeftBtnUp;
                    img.MouseRightButtonDown += sceneObject_MouseRightBtnDown;
                    img.MouseRightButtonUp += sceneObject_MouseRightButtonUp;
                    //img.MouseLeave += sceneObject_MouseLeave;
               
                    Canvas.SetLeft(img, so.Position.X);
                    Canvas.SetTop(img, so.Position.Y);

                    // Set some bindings
                    //this.setBindings(img, so.Size.Width, so.Size.Height, so.Position.X, so.Position.Y);

                    this.sceneObjects.Add(so);
                    this.ctx.Children.Add(img);
                }

                RaisePropertyChanged("sceneObjects");
            }
            catch (Exception ex)
            {
                Debug.dd("Create Scene", ex.Message);
                MessageBox.Show("Creating scene in canvas from file failed! " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        void t_DragDelta(object sender, DragDeltaEventArgs e)
        {
            
        }

       
        #endregion

        #region GET COLOR FROM NAME
        private static Color FromName(string bgColor)
        {
            return (Color)System.Windows.Media.ColorConverter.ConvertFromString(bgColor);
        }
        #endregion

        #region SET BACKGROUND COLOR
        public void SetBackground(Color c)
        {
            this.ctx.Background = new SolidColorBrush(c);
            XMLDocManager doc = new XMLDocManager();
            doc.SetBackgroundColor(c.ToString());
        }
        #endregion

        /**************************************** CANVAS MOUSE EVENTS ***************************************************/

        #region DROP ON CANVAS
        public void DropOnCanvas(Canvas canvas, Point mPos, ImageSource bitmap)
        {
            try
            {
                XMLDocManager doc = new XMLDocManager();
                int id = doc.GetCount() + 1;

                Rectangle img = new Rectangle();
                img.Fill = new ImageBrush(bitmap);
                img.Uid = id.ToString();
                img.Width = bitmap.Width;
                img.Height = bitmap.Height;
                img.IsHitTestVisible = true;

                //img.MouseLeftButtonDown += sceneObject_MouseLeftBtnDown;
                //img.MouseLeftButtonUp += sceneObject_MouseLeftBtnUp;
                img.MouseRightButtonDown += sceneObject_MouseRightBtnDown;
                img.MouseRightButtonUp += sceneObject_MouseRightButtonUp;
                //img.MouseLeave += sceneObject_MouseLeave;

                Size imgSize = new Size { Width = bitmap.Width, Height = bitmap.Height };

                double leftPos = mPos.X - (img.Width / 2);
                double topPos = mPos.Y - (img.Height / 2);

                // Set some bindings
                //this.setBindings(img, bitmap.Width, bitmap.Height, leftPos, topPos);
               
                Point imgPos = new Point { X = leftPos, Y = topPos };
                string name = System.IO.Path.GetFileNameWithoutExtension(bitmap.ToString());
                string src = Directory.GetParent(@"../../").FullName + @"\assets\" + Globals.Name + @"\" + name + System.IO.Path.GetExtension(bitmap.ToString());

                SceneObjectViewModel so = new SceneObjectViewModel { ID = id, Name = name + id, Source = src, Type = 0, Size = imgSize, Position = imgPos };

                this.sceneObjects.Add(so);
               
                doc.CreateSceneObjectNodes(so);

                Canvas.SetLeft(img, leftPos);
                Canvas.SetTop(img, topPos);
                this.ctx.Children.Add(img);

                AddSelectBox();
            }
            catch (Exception ex)
            {
                Debug.dd("DropOnCanvas", ex.Message);
                MessageBox.Show("Upload to canvas failed!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
       
        #endregion

        #region CANVAS MOUSE ENTER
        public void CanvasMouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (e.Source != this.ctx && e.Source.GetType() == typeof(Rectangle))
            {
                selectedElement = e.Source as Rectangle;
                selectedElement.Stroke = Brushes.Blue;
            }
        }
        #endregion

        #region SHOW SELECTION BORDERS
        public void ShowSelectionBorder(System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.Source != this.ctx && e.Source.GetType() == typeof(Rectangle))
            {
                //selectedElement = e.Source as Rectangle;
                (e.Source as Rectangle).Stroke = Brushes.Blue;
            }
        }
        #endregion

        #region HIDE SELECTION BORDERS
        public void HideSelectionBorder(System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.Source != this.ctx && e.Source.GetType() == typeof(Rectangle))
            {
                //selectedElement = e.Source as Rectangle;
                (e.Source as Rectangle).Stroke = Brushes.Transparent;
            }
        }
        #endregion

        #region HIDE SELECTION BORDERS
        public void HideSelectionBorder(System.Windows.Input.MouseEventArgs e)
        {
            if (e.Source != this.ctx && e.Source.GetType() == typeof(Rectangle))
            {
                selectedElement = e.Source as Rectangle;
                selectedElement.Stroke = Brushes.Transparent;
            }
        }
        #endregion

        #region CANVAS MOUSE LEFT BTN DOWN
        public void CanvasMouseLeftBtnDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {

            if (selectedElement != null)
            {
                this.HideSelectionBorder(e);
            }
            else
            {
                this.ShowSelectionBorder(e);
            }

            if ( e.Source.GetType() == typeof(Rectangle))
            {
                if (selectedElement == null)
                {
                    // Get mouse position onMouseDown
                    UIElement s = sender as UIElement;
                    originalZIndex = (Canvas.GetZIndex(s) < 0) ? Canvas.GetZIndex(s) : 0;

                    this.SetMouseSelectionSettings(sender, e);
                   
                }
            }
            else if ( e.Source.GetType() == typeof(Canvas)) {
                selectedElement = null;
                selected = false;
            }

            anchorPoint.X = e.GetPosition(this.ctx).X;
            anchorPoint.Y = e.GetPosition(this.ctx).Y;
            isMouseDown = true;
            isMouseDragging = true;

            if (!selected && selectedElement == null && dragSelectBox != null)
            {
                // Set/Reset Selection Box attributes;
                dragSelectBox.Width = 0;
                dragSelectBox.Height = 0;
                dragSelectBox.SetValue(Canvas.LeftProperty, anchorPoint.X);
                dragSelectBox.SetValue(Canvas.TopProperty, anchorPoint.Y);
            }
        }

        #endregion

        #region PREVIEW CANVAS MOUSE LEFT BTN DOWN
        public void PreviewCanvasMouseLeftBtnDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {

            //this.SetMouseSelectionSettings(sender, e);
        }
        #endregion

        #region CANVAS MOUSE LEFT BTN UP
        public void CanvasMouseLeftBtnUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            //if(e.Source.GetType() == typeof(Rectangle)) this.clearBindings(sender as Rectangle);
            this.StopDragging();
            if (e.Source.GetType() == typeof(Rectangle)) { Rectangle r = sender as Rectangle; this.sceneObjects.ToList().Where(rec => rec.ID == Int32.Parse(r.Uid)).Select(s => { s.Position = new Point { X = Canvas.GetLeft(r), Y = Canvas.GetTop(r) }; return s; }); }
        }

        #endregion

        #region DRAG FINISHED PREVIEW MOUSE HANDLER
        // Handler for drag stopping on user choise
        public void DragFinishedMouseHandler(object sender, MouseButtonEventArgs e)
        {
            this.HideSelectionBorder(e);
            //this.UpdateSceneObjects();
            this.StopDragging();
            e.Handled = true;
        }
        #endregion

        #region CANVAS MOUSE LEAVE
        public void CanvasMouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (e.Source.GetType() == typeof(Rectangle))
            {
                selectedElement = sender as Rectangle;
                selectedElement.Stroke = Brushes.Transparent;
                this.ChangeZIndex(sender, e, originalZIndex);
            }

            //this.UpdateSceneObjects();
            this.StopDragging();
            e.Handled = true;
        }
        #endregion

        #region CANVAS MOUSE MOVE
        public void CanvasMouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (_isDown)
            {
                if ((_isDragging == false) &&
                    ((Math.Abs(e.GetPosition(this.ctx).X - _startPoint.X) > SystemParameters.MinimumHorizontalDragDistance) ||
                    (Math.Abs(e.GetPosition(this.ctx).Y - _startPoint.Y) > SystemParameters.MinimumVerticalDragDistance)))
                    _isDragging = true;

                if (_isDragging)
                {
                    this.Cursor = Cursors.ScrollAll;
                    Point position = Mouse.GetPosition(this.ctx);
                    Point canvPosToWindow = this.ctx.TransformToAncestor(this.ctx).Transform(new Point(0, 0));

                    Rectangle r = e.Source as Rectangle;

                    try
                    {
                        double upperlimit = canvPosToWindow.Y + (r.ActualHeight / 2);
                        double lowerlimit = canvPosToWindow.Y + this.ctx.ActualHeight - (r.ActualHeight / 2);

                        double leftlimit = canvPosToWindow.X + (r.ActualWidth / 2);
                        double rightlimit = canvPosToWindow.X + this.ctx.ActualWidth - (r.ActualWidth / 2);

                        double absmouseXpos = e.GetPosition(this.ctx).X;
                        double absmouseYpos = e.GetPosition(this.ctx).Y;

                        if ((absmouseXpos > leftlimit && absmouseXpos < rightlimit)
                            && (absmouseYpos > upperlimit && absmouseYpos < lowerlimit))
                        {
                            this.Cursor = Cursors.ScrollAll;
                            double flGoalX = e.GetPosition(this.ctx).X - (r.ActualWidth / 2);
                            double flGoalY = e.GetPosition(this.ctx).Y - (r.ActualHeight / 2);
                            double flcurrentLeft = Canvas.GetLeft(r);
                            double flcurrentTop = Canvas.GetTop(r);
                            double dt = 30;

                            //double x = Maths.Lerp(flGoalX, flcurrentLeft, dt * 20);
                            //double y = Maths.Lerp(flGoalY, flcurrentTop, dt * 20);

                            // Set the box Position, width and height
                            //int zindex = this.ctx.Children.Count + 1;
                            //r.SetValue(Canvas.ZIndexProperty, 1000);
                            r.SetValue(Canvas.LeftProperty, Maths.Lerp(flGoalX, flcurrentLeft, dt * 20));
                            r.SetValue(Canvas.TopProperty, Maths.Lerp(flGoalY, flcurrentTop, dt * 20));

                            //Or use this                            
                            //Canvas.SetTop(selectedElement, Maths.Lerp(position.Y - (_startPoint.Y - _originalTop), flcurrentLeft, dt * 20));
                            //Canvas.SetLeft(selectedElement, Maths.Lerp(position.X - (_startPoint.X - _originalLeft), flcurrentTop, dt * 20));
                        }
                    }
                    catch (NullReferenceException ex)
                    {
                        Debug.dd("Scene.CanvasMouseMove()", ex.Message);
                    }

                }
            }

            if (!selected)
            {
                if (selectedElement == null && dragSelectBox != null)
                {
                    this.setDragSelectBox(sender, e);
                }
            }

        }
        #endregion

        #region CANVAS MOUSE RIGHT CLICK SHOW CONTEXT MENU
        public void ShowContextMenu(object sender, MouseButtonEventArgs e)
        {
            if (e.Source.GetType() == typeof(Canvas))
            {
                (e.Source as Canvas).ContextMenu = MainWindow.main.FindResource("CanvasMenu") as ContextMenu;
            }
        }
        #endregion

        #region SET DRAG SELECT BOX
        public void setDragSelectBox(object sender, System.Windows.Input.MouseEventArgs e)
        {
            this.mousePos = e.GetPosition(this.ctx);

            if (isMouseDown && isMouseDragging && !Mode.Pan)
            {
                double x = 0.0, y = 0.0, width = 0, height = 0;
                Point point2 = e.GetPosition(this.ctx);

                // Set all case values correctly

                // left
                if (point2.X < anchorPoint.X)
                {
                    x = point2.X;
                    width = anchorPoint.X - point2.X;
                }
                else
                {
                    x = anchorPoint.X;
                    width = point2.X - anchorPoint.X;
                }

                //up
                if (point2.Y < anchorPoint.Y)
                {
                    y = point2.Y;
                    height = anchorPoint.Y - point2.Y;
                }
                else
                {
                    y = anchorPoint.Y;
                    height = point2.Y - anchorPoint.Y;
                }

                if (width < 0) { width = 0; }
                if (height < 0) { height = 0; }

                // Interpolation params
                double flGoalX = x;
                double flGoalY = y;
                double flcurrentLeft = Canvas.GetLeft(dragSelectBox);
                double flcurrentTop = Canvas.GetTop(dragSelectBox);
                double dt = 30;

                // Set the box Position, width and height
                dragSelectBox.SetValue(Canvas.LeftProperty, Maths.Lerp(flGoalX, flcurrentLeft, dt * 20));
                dragSelectBox.SetValue(Canvas.TopProperty, Maths.Lerp(flGoalY, flcurrentTop, dt * 20));

                dragSelectBox.Width = width;
                dragSelectBox.Height = height;

                double lblWidthPosX = Canvas.GetLeft(dragSelectBox) + width / 2 - 10;
                double lblWidthPosY = Canvas.GetTop(dragSelectBox) - 20;
                double lblHeightPosX = Canvas.GetLeft(dragSelectBox) - 30;
                double lblHeightPosY = Canvas.GetTop(dragSelectBox) + height / 2 - 10;

                lblWidth.Content = Math.Round(Math.Abs(width), 0).ToString() + "px";
                lblHeight.Content = Math.Round(Math.Abs(height), 0).ToString() + "px";

                lblWidth.SetValue(Canvas.LeftProperty, lblWidthPosX);
                lblWidth.SetValue(Canvas.TopProperty, lblWidthPosY);
                lblHeight.SetValue(Canvas.LeftProperty, lblHeightPosX);
                lblHeight.SetValue(Canvas.TopProperty, lblHeightPosY);

                // Show the box
                if (dragSelectBox.Visibility != Visibility.Visible)
                {
                    dragSelectBox.Stroke = Brushes.DarkBlue;
                    dragSelectBox.Visibility = Visibility.Visible;
                    lblWidth.Visibility = Visibility.Visible; lblHeight.Visibility = Visibility.Visible;
                }

            }
        }
        #endregion

        #region ZOOM
        public void Zoom(object sender, System.Windows.Input.MouseWheelEventArgs e)
        {

            if (Mode.Zoom == true)
            {
                //scroller.CanContentScroll = false;
                zoom += zoomSpeed * e.Delta; // Ajust zooming speed (e.Delta = Mouse spin value )
                if (zoom < zoomMin) { zoom = zoomMin; } // Limit Min Scale
                if (zoom > zoomMax) { zoom = zoomMax; } // Limit Max Scale

                Point mousePos = e.GetPosition(this.ctx);

                if (zoom > 1)
                {
                    this.ctx.RenderTransform = new ScaleTransform(zoom, zoom, mousePos.X, mousePos.Y); // transform Canvas size from mouse position
                }
                else
                {
                    this.ctx.RenderTransform = new ScaleTransform(zoom, zoom); // transform Canvas size
                }
            }
            e.Handled = true;
        }
        #endregion

        #region SCROLLER MOUSE EVENTS
        public void ScrollerMouseLeftBtnDown(object sender, MouseButtonEventArgs e)
        {
            if (Mode.Pan)
            {
                scrollMousePoint = e.GetPosition(this.sv);
                hOff = this.sv.HorizontalOffset;
                vOff = this.sv.VerticalOffset;
                this.sv.CaptureMouse();
            }
        }

        public void ScrollerMouseLeftBtnUp(object sender, MouseButtonEventArgs e)
        {
            scrollMousePoint = e.GetPosition(this.sv);
            this.Cursor = Cursors.Arrow;
            this.sv.ReleaseMouseCapture();
        }

        public void ScrollerMouseMove(object sender, MouseEventArgs e)
        {
            if (Mode.Pan && this.sv.IsMouseCaptured)
            {
                this.Cursor = Cursors.Arrow;
                this.sv.ScrollToHorizontalOffset(hOff + (scrollMousePoint.X - e.GetPosition(this.sv).X));
                this.sv.ScrollToVerticalOffset(vOff + (scrollMousePoint.Y - e.GetPosition(this.sv).Y));
            }
        }

        #endregion

        #region SET MOUSE SELECTION SETTINGS
        // Initialize Mouse move Adorner settings
        public void SetMouseSelectionSettings(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            // Remove selection on clicking anywhere the window
            //if (selected)
            //{
            //    selected = false;
            //    if (selectedElement != null)
            //    {
            // Remove the adorner from the selected element
            //aLayer.Remove(aLayer.GetAdorners(selectedElement)[0]);
            //selectedElement = null;
            //}
            //}

            // If any element except canvas is clicked, 
            // assign the selected element and add the adorner

            //if (e.Source != this.ctx && e.Source.GetType() == typeof(Image))
            //{
            if (e.Source != this.ctx && e.Source.GetType() == typeof(Rectangle))
            {
                _isDown = true;
                _startPoint = e.GetPosition(this.ctx);

                //selectedElement = e.Source as Image;
                selectedElement = e.Source as Rectangle;

                _originalLeft = Canvas.GetLeft(selectedElement);
                _originalTop = Canvas.GetTop(selectedElement);

                //aLayer = AdornerLayer.GetAdornerLayer(selectedElement);
                //aLayer.Add(new ResizingAdorner(selectedElement));
                selected = true;
                _isDown = true;
                //this.ChangeZIndex(sender, e, this.ctx.Children.Count + 1);
                e.Handled = true;
            }
        }
        #endregion

        #region STOP DRAGGING
        // Method for stopping dragging
        private void StopDragging()
        {
            this.Cursor = Cursors.Arrow;
            isMouseDown = false;
            isMouseDragging = false;
            if (dragSelectBox != null)
            {
                dragSelectBox.Visibility = Visibility.Collapsed;
                lblWidth.Visibility = Visibility.Collapsed;
                lblHeight.Visibility = Visibility.Collapsed;
            }
            selectedElement = null;
            _isDown = false;
            _isDragging = false;

            //this.ctx.ReleaseMouseCapture();
        }

        #endregion

        /****************************************  CANVAS CHILD MOUSE EVENTS ***************************************************/

        #region SCENE OBJECT MOUSE LEFT BUTTON DOWN 
        private void sceneObject_MouseLeftBtnDown(object sender, MouseButtonEventArgs e)
        {
            selectedElement = sender as Rectangle;
            this.SetMouseSelectionSettings(sender, e);
            //e.Handled = true;
        }

        #endregion

        #region SCENE OBJECT MOUSE LEFT BUTTON UP
        private void sceneObject_MouseLeftBtnUp(object sender, MouseButtonEventArgs e)
        {
            //this.ChangeZIndex(sender, e, originalZIndex);
            //e.Handled = true;

        }
        #endregion

        #region SCENE OBJECT MOUSE LEAVE
        private void sceneObject_MouseLeave(object sender, MouseEventArgs e)
        {
            //
        }

        #endregion

        #region SCENE OBJECT MOUSE RIGHT BTN DOWN
        private void sceneObject_MouseRightBtnDown(object sender, MouseButtonEventArgs e)
        {
            selectedElement = sender as Rectangle;
            this.ShowSelectionBorder(e);
            (sender as FrameworkElement).ContextMenu = MainWindow.main.FindResource("SceneObjectMenu") as ContextMenu;
        }
        #endregion

        #region SCENE OBJECT MOUSE RIGHT BUTTON UP
        private void sceneObject_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            //selectedElement = null;
            this.HideSelectionBorder(e);
        }

        #endregion

        /**************************************** END CANVAS MOUSE EVENTS ***************************************************/

        #region SET BINDINGS
        private void setBindings(Rectangle img, double W, double H, double X, double Y)
        {
            //Binding bW = new Binding();
            //Binding bH = new Binding();
            //Binding bX = new Binding();
            //Binding bY = new Binding();

            //// Width
            //bW.ElementName = "assetWidth";
            //bW.StringFormat = "{}{0:0}}";
            //bW.Path = new PropertyPath("Text");
            //bW.Mode = BindingMode.TwoWay;
            
            //// Height
            //bH.ElementName = "assetHeight";
            //bH.StringFormat = "{}{0:0}}";
            //bH.Path = new PropertyPath("Text");
            //bH.Mode = BindingMode.TwoWay;

            //// X Position
            //bX.ElementName = "assetPosX";
            //bX.StringFormat = "{}{0:0}}";
            //bX.Path = new PropertyPath("Canvas.Left");
            //bX.Mode = BindingMode.TwoWay;

            //// Y Position
            //bY.ElementName = "assetPosY";
            //bY.StringFormat = "{}{0:0}}";
            //bY.Path = new PropertyPath("Canvas.Top");
            //bY.Mode = BindingMode.TwoWay;
            
            //// Set Fallback values
            //bW.FallbackValue = W;
            //bH.FallbackValue = H;
            //bX.FallbackValue = X;
            //bY.FallbackValue = Y;

            //img.SetBinding(WidthProperty, bW);
            //img.SetBinding(HeightProperty, bH);
            //img.SetBinding(Canvas.LeftProperty, bX);
            //img.SetBinding(Canvas.TopProperty, bY);

            //MainWindow.main.assetWidth.Text = W.ToString();
        }
        #endregion

        #region CLEAR BINDINGS
        private void clearBindings(Rectangle rectangle)
        {
            rectangle.InputBindings.Clear();
        }
        #endregion

        #region CHANGE CANVAS CHILD Z-INDEX
        private void ChangeZIndex(Object sender, MouseButtonEventArgs e, int value)
        {
            if (e.Source != this.ctx && e.Source.GetType() == typeof(Rectangle))
            {
                UIElement r = sender as UIElement;
                Canvas.SetZIndex(r, value);
            }
        }

        #endregion

        #region CHANGE CANVAS CHILD Z-INDEX
        private void ChangeZIndex(Object sender, MouseEventArgs e, int value)
        {
            if (e.Source != this.ctx && e.Source.GetType() == typeof(Rectangle))
            {
                UIElement r = sender as UIElement;
                Canvas.SetZIndex(r, value);
            }
        }

        #endregion

        #region UPDATE SCENE OBJECTS
        public void UpdateSceneObjects()
        {
            //IEnumerable<Rectangle> sceneItems = MainWindow.main.canvas.Children.OfType<Rectangle>();

            //ObservableCollection<SceneObjectViewModel> sobjects = new ObservableCollection<SceneObjectViewModel>();
            try
            {
            //    foreach (SceneObjectViewModel so in this.sceneObjects.ToList())
            //    {
            //        foreach (FrameworkElement item in sceneItems)
            //        {
            //            if (item.Uid != null && so.ID.ToString() == item.Uid)
            //            {
            //                so.Size = new Size { Width = item.ActualWidth, Height = item.ActualHeight };
            //                so.Position = new Point { X = Canvas.GetLeft(item), Y = Canvas.GetTop(item) };
            //            }
            //            else
            //            {
            //                sobjects.Add(so);
            //            }
            //        }
            //    }

            //    foreach (SceneObjectViewModel item in sobjects)
            //    {
            //        sceneObjects.Remove(item);
            //        MainWindow.main.sceneCollection.Remove(item);
            //    }

            }
            catch (Exception e)
            {
                Debug.dd("UpdateSceneObjects: ", e.Message);
                MessageBox.Show("A problem occured while updating the Scene Object. " + e.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        #endregion

        #region GET SCENE OBJECTS
        public ObservableCollection<SceneObjectViewModel> GetSceneObjects()
        {
            return this.sceneObjects;
        }
        #endregion

        #region DESTROY SCENE
        /// <summary>
        /// Dispose of all scene objects
        /// </summary>
        public void Destroy()
        { }
        #endregion

        #region EVENT HANDLERS
        /// <summary>
        /// Raise Property Changed Event
        /// </summary>
        /// <param name="propName"></param>
        public void RaisePropertyChanged(string propName)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }
        #endregion

        public ICommand AddNewObject { get; set; }

        public ICommand ChangeBackground { get; set; }

        public ICommand ClearScene { get; set; }

        #region DELETE OBJECT 
        public void DeleteObject(int index)
        {
            try
            {
                XMLDocManager doc = new XMLDocManager();
                doc.DeleteNode(index);
                this.sceneObjects.ToList().RemoveAll(r => r.ID == index);
            }
            catch (Exception e) {
                Debug.dd("Delete Object ", e.Message);
            }
        }
        #endregion

    }
}
