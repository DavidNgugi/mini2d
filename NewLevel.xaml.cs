﻿using System;
using System.Windows;
using System.Windows.Controls;
using MahApps.Metro.Controls;

namespace mini2d_demo
{
    /// <summary>
    /// Interaction logic for NewLevel.xaml
    /// </summary>
    public partial class NewLevel : MetroWindow
    {
        Scene scene;
        LevelViewModel level;
        public string lName;
        public string lOrder;
        public int lWidth;
        public int lHeight;

        public NewLevel()
        {
            InitializeComponent();
            scene = new Scene();
            levelWidth.Text = "1000";
            levelHeight.Text = "500";
        }

        private void btnCreateLevel_Click_1(object sender, RoutedEventArgs e)
        {
            XMLDocManager xmld = new XMLDocManager();

            if (String.IsNullOrEmpty(levelName.Text) || string.IsNullOrWhiteSpace(levelName.Text))
            {
                MessageBox.Show("Provide a valid level name!");
            }
            else
            {
                this.lName = levelName.Text;
                this.lOrder = ((ListBoxItem)levelOrder.SelectedItem).Name.ToString();
                this.lWidth =  int.Parse(levelWidth.Text);
                this.lHeight = int.Parse(levelHeight.Text);

                level = new LevelViewModel{ Name = this.lName, Order = this.lOrder, Width = this.lWidth, Height = this.lHeight};
                
                // Create the Project XML file
                if (xmld.CreateNewlevel(level))
                {
                    Globals.LevelCount++;
                    scene.Create(level);
                    MainWindow.main.CanvasItem.Width = level.Width;
                    MainWindow.main.CanvasItem.Width = level.Height;
                    MainWindow.main.CanvasItem.IsEnabled = true;
                    MainWindow.main.CanvasItem.Visibility = Visibility.Visible;
                    MessageBox.Show("New Level Created successfully!");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("A problem occured!");
                }
            }
        }

        private void btnCancel_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
