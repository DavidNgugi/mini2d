﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mini2d_demo
{
    public static class Mode
    {
        public static bool Pan { get; set; }
        public static bool Select { get; set; }
        public static bool Zoom { get; set; }

        public static void Reset()
        {
            Pan = false;
            Select = false;
            Zoom = false;
        }

        public static void Status()
        {
            //Debug.dd("Modes - Pan, Select, Zoom", Pan, Select, Zoom);
        }
    }
}
